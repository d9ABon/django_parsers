"use strict";
var system = require('system');
var page = require('webpage').create();
var query = system.args[1].trim();
//var proxy = system.args[2].trim();


page.settings.userAgent = 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0';
// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
page.onInitialized = function () {
    page.evaluate(function () {
        (function () {
            window.navigator = JSON.parse("{\"permissions\":{},\"mimeTypes\":{},\"plugins\":{},\"doNotTrack\":\"unspecified\",\"maxTouchPoints\":0,\"oscpu\":\"Linux x86_64\",\"vendor\":\"\",\"vendorSub\":\"\",\"productSub\":\"20100101\",\"cookieEnabled\":true,\"buildID\":\"20180412172954\",\"mediaDevices\":{},\"serviceWorker\":{\"controller\":null},\"credentials\":{},\"webdriver\":false,\"hardwareConcurrency\":4,\"geolocation\":{},\"appCodeName\":\"Mozilla\",\"appName\":\"Netscape\",\"appVersion\":\"5.0 (X11)\",\"platform\":\"Linux x86_64\",\"userAgent\":\"Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0\",\"product\":\"Gecko\",\"language\":\"ru\",\"languages\":[\"ru\",\"en-US\",\"en\"],\"onLine\":true,\"storage\":{}}");
            window.navigator.cookieEnabled = true;
        })();
    });
};

page.onError = function(msg, trace) {
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    system.stderr.writeLine(msgStack.join('\n'));
};

//phantom.setProxy(proxy.split(':')[0], proxy.split(':')[1], 'manual', '', '');

page.open('https://duckduckgo.com/?q='+encodeURIComponent(query), function(status) {

    //console.log(query);phantom.exit();
    if ( status !== "success" ) {
        phantom.exit();
    }

    //page.includeJs("https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", function() {
        window.setTimeout(function () {

            page.vqd = page.evaluate(function(){
                return window.vqd;
            });
            page.snippets = page.evaluate(function(){
                var snippets = [], i;
                var elems = document.getElementsByClassName('result__snippet');
                for (i=0; i<elems.length; i++) {
                    var text = elems[i].textContent || elems[i].innerText || "";
                    snippets.push(text);
                }
                return snippets;
            });

            page.open('https://duckduckgo.com/i.js?q='+encodeURIComponent(query)+'&vqd='+page.vqd+'&sp=0&iaf=size%3Aimagesize-large', function(status) {
                var jsonStr = page.plainText;

                try {
                    var jsonObj = JSON.parse(jsonStr);
                    jsonObj['snippets'] = page.snippets;
                    jsonStr = JSON.stringify(jsonObj);
                } catch (e) {
                    //pass
                }

                system.stdout.writeLine(jsonStr);
                //console.log(jsonStr);
                phantom.exit();
            });


        }, 2000);
    //});



});



//page.render('ddg.png');
//console.log(page.evaluate(function () {
//    return document.documentElement.outerHTML;
//}));
