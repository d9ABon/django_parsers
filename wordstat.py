# -*- coding: utf-8 -*-
from base64 import b64encode
from hashlib import md5
import re
import os
import sys
import time
import json
import random
from hashlib import md5
import base64
import subprocess
from pprint import pprint
from django.core.management.commands import shell

from lib.php import php
from lib.proxy import Proxy
from lib.decorators import profile_fun

from django.core.cache import cache

from settings import PROJECT_PATH



def get_cookie():
    cookie_fuid01 = None

    dirpath, dirnames, filenames = list(os.walk(os.path.join(PROJECT_PATH, 'parsers', 'wordstat_cookies')))[0]
    cookie_file = os.path.join(PROJECT_PATH, 'parsers', 'wordstat_cookies', random.choice(filenames))
    print 'using cookie file:', cookie_file
    f = open(cookie_file)
    cookies = f.readlines()
    f.close()
    cookies = map(lambda s: s.strip(), cookies)
    cookies = filter(len, cookies)
    for i, cookie in enumerate(cookies):
        cookie = cookie.split("\t")
        cookie = map(lambda s: s.strip(), cookie)
        cookie = filter(len, cookie)
        host, flag, path, secure, expires, name, value = cookie
        cookie = {'host': host, 'secure': bool(secure.title()),  #title(): TRUE => True
                  'name': name, 'value': value, 'path': path, 'expires': int(expires), }
        if cookie['name'] == 'fuid01':
            cookie_fuid01 = cookie['value']
        cookies[i] = cookie
        #print 'Cookielist:', cookies[i]

    useragent = 'Mozilla/5.0 (X11; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0'

    return cookies, useragent, cookie_fuid01


class WordstatError(Exception): pass

@profile_fun
def get_keywords(query):
    if isinstance(query, unicode):
        query = query.encode('utf-8')
    cache_key = 'wordstat_%s' % md5(query).hexdigest()
    results = cache.get(cache_key)
    if results is not None:
        return results


    cookies, useragent, fuid01 = get_cookie()
    args = [
        'cookies=B64_%s' % base64.b64encode(json.dumps(cookies)),
        'useragent=B64_%s' % base64.b64encode(useragent),
        'fuid01=B64_%s' % base64.b64encode(fuid01),
        'words=%s' % query,
    ]
    args = ['node', '%s/parsers/npm_wordstat/app' % PROJECT_PATH] + args
    #print 'args:', ' '.join(args)
    process = subprocess.Popen(args, stdout=subprocess.PIPE, shell=False)
    output, unused_err = process.communicate()
    try:
        content = json.loads(output)
    except ValueError as e:
        raise WordstatError(output)

    #print 'content', content

    def _by_attr(data, attr_name, attr_value):
        ret_data = []
        if isinstance(data, dict):
            if attr_name in data and data[attr_name] == attr_value:
                ret_data += [data]
            elif 'content' in data:
                return _by_attr(data['content'], attr_name, attr_value)
        elif isinstance(data, list):
            for val in data:
                ret_data += _by_attr(val, attr_name, attr_value)

        return ret_data

    table = _by_attr(content, 'elem', 'table')[0]
    phrases = _by_attr(table, 'elem', 'phrases')

    more_with_this_keyword = _by_attr(phrases[0], 'elem', 'table')[0]
    similar_keywords = _by_attr(phrases[1], 'elem', 'table')[0]

    more_with_this_keyword = _by_attr(more_with_this_keyword, 'tag', 'tr')[1:]  #first is header
    similar_keywords = _by_attr(similar_keywords, 'tag', 'tr')[1:]  #first is header

    out_data = {'more_with_this_keyword': {}, 'similar_keywords': {}}

    for word_data in more_with_this_keyword:
        word_data = _by_attr(word_data, 'tag', 'td')
        word = _by_attr(word_data, 'block', 'b-phrase-link')[0]['content']
        count = _by_attr(word_data, 'elem', 'td-count')[0]['content'].replace(' ', '')
        count = re.sub(r'[^\d]+', r'', count)  #remove not number chars
        out_data['more_with_this_keyword'][word] = int(count)
        #print word, count

    #print '-'*30

    for word_data in similar_keywords:
        word_data = _by_attr(word_data, 'tag', 'td')
        word = _by_attr(word_data, 'block', 'b-phrase-link')[0]['content']
        count = _by_attr(word_data, 'elem', 'td-count')[0]['content'].replace(' ', '')
        count = re.sub(r'[^\d]+', r'', count)  #remove not number chars
        out_data['similar_keywords'][word] = int(count)
        #print word, count


    if len(out_data['more_with_this_keyword']) or len(out_data['similar_keywords']):
        cache.set(cache_key, out_data, 60 * 60 * 24 * 365)

    #import ipdb;ipdb.set_trace();pass
    #print output, unused_err
    return out_data
