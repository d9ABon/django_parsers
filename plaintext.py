# -*- coding: utf-8 -*-
import random
import re
import os
import time
import json
import requests
import subprocess

from HTMLParser import HTMLParser
from bs4 import BeautifulSoup  # Or from BeautifulSoup import BeautifulSoup
import htmlentitydefs

from math import floor
from hashlib import md5
from django.core.cache import cache
from multiprocessing import Pool

try:
    from settings import PROJECT_PATH, REQUESTERS
except ImportError:
    from project.settings import PROJECT_PATH, REQUESTERS

from lib.base import uniq, slugify, chunks, sentences, pr, prcolored, urlencode
from lib.decorators import time_limit, TimeoutException, profile_fun, print_prof_data, DisableLogger
from lib.progress_meter import ProgressMeter

from readability.readability import Document as ReadabilityDocument, Unparseable  #pip install readability-lxml



mercury_tokens = (
    'WEn1p5SXr2f12D1LVp6qsOO1IuXeLS2YtMJA6WBj',
    'tIZ1NkNWqHBCpTil5dqIUdkwNymCAAR22oHD5Y15',
    'oKBaeiySinNI93brWivkoOnLDWAXN6SWzyxoEo1x',
    'HhDwyzyfdTEGsemb5KDXPhwPuD7c1upvDRmi9KE3',
    'BMpVN6CVr1ngukJn484Efr5GXfbRtT5RByeOUDl1',
)

PLAINTEXT_MIN_TEXT_LENGTH = 5


with open(os.path.join(PROJECT_PATH, 'parsers', 'useragents.txt')) as f:
    useragents = filter(len, map(lambda line: line.decode('utf-8').strip(), f.readlines()))


with open(os.path.join(PROJECT_PATH, 'parsers', 'html_tags.txt')) as f:
    html_tags = filter(len, map(lambda line: line.decode('utf-8').lower().strip(), f.readlines()))


with open(os.path.join(PROJECT_PATH, 'parsers', 'plaintext_stopwords.txt')) as f:
    stopwords = filter(len, map(lambda line: line.decode('utf-8').lower().strip(), f.readlines()))


def stopwords_check_passed(line):
    line = line.lower()
    for sw in stopwords:
        if sw[0] == sw[-1] == '|':  #stopword is regexp
            sw = sw[1:-1]
            if sw[0] != '^':
                sw = '.*' + sw
            if sw[-1] != '$':
                sw += '.*'
            if re.match(sw, line):
                #print 'SW!', sw, line
                return False
        elif sw in line:
            #print 'SW!', sw, line
            return False
    return True


def mercury(url):
    #url = 'http://ru.savefrom.net/1-быстрый-способ-скачать-с-youtube/'
    token = random.choice(mercury_tokens)

    parseurl = "https://mercury.postlight.com/parser?url=%s" % urlencode(url)

    res = None
    try:
        res = requests.get(parseurl, timeout=15, headers={'x-api-key': token}).content
        html = json.loads(res)['content']
        return sanitize_html(HTMLParser().unescape(html))
    except Exception as e:
        print res, e
        return ''

def python_readability(url):
    if 'vk.com' in url or 'youtube.com' in url or '.pdf' in url:
        return ''

    try:
        html = requests.get(url, timeout=8, headers={'User-Agent': random.choice(useragents)}).content
    except Exception as e:
        return ''

    with DisableLogger():
        with time_limit(8):
            try:
                doc = ReadabilityDocument(html, min_text_length=PLAINTEXT_MIN_TEXT_LENGTH)
                html = doc.summary()
                # print 'url', url, 'text:', text
                readable_title = doc.short_title()
            except Unparseable:
                print 'Parsing error', url
                return ''
            except TimeoutException:
                print 'python_readability TimeoutException'
                return ''

    try:
        return sanitize_html(HTMLParser().unescape(html))
    except Exception as e:
        print e
        return ''


def plaintext_through_requester(url):
    req_url = random.choice(REQUESTERS) + 'plaintext/?url=' + urlencode(url)

    with time_limit(22):
        try:
            return requests.get(req_url, timeout=21).content
        except TimeoutException:
            return ''
        except Exception as e:
            return ''







class HTMLTextExtractor(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.result = [ ]
    def handle_data(self, d):
        self.result.append(d)
    def handle_charref(self, number):
        codepoint = int(number[1:], 16) if number[0] in (u'x', u'X') else int(number)
        self.result.append(unichr(codepoint))
    #def handle_entityref(self, name):
    #    codepoint = htmlentitydefs.name2codepoint[name]
    #    self.result.append(unichr(codepoint))
    def get_text(self):
        return u''.join(self.result)
def strip_tags(html):
    html = ''.join(BeautifulSoup(html, 'lxml').findAll(text=True))

    html = html.decode('utf-8')
    s = HTMLTextExtractor()
    s.feed(html)
    res = s.get_text()

    #re.sub('<[^<]+?>', '', text)
    return res


def strip_remaining_tags(line):
    line = re.compile(r'<[(%s)][^>]*>' % ")(".join(html_tags)).sub('', line)
    for tag in html_tags:
        try:
            line = line.replace('</%s>' % tag, '')
        except UnicodeDecodeError as e:
            line = line.decode('utf-8').replace('</%s>' % tag, '').encode('utf-8')
    return line


def char_types_count(text):
    digits = '0123456789'
    latin = 'abcdefghijklmnopqrstuvwxyz'
    latin += latin.upper()
    cyrilic = u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    cyrilic += cyrilic.upper()
    cyrilic = cyrilic#.encode('utf-8')
    sp_chars = """ .,'"<>/`~!@#$%^&*()_+=-:;[]{}|\\"""

    len_text = len(text)
    len_digits = len(filter(lambda x: x in digits, text))
    len_latin = len(filter(lambda x: x in latin, text))
    try:
        len_cyrilic = len(filter(lambda x: x in cyrilic, text))
    except UnicodeDecodeError:
        try:
            len_cyrilic = len(filter(lambda x: x.encode('utf-8') in cyrilic, text))
        except UnicodeDecodeError:
            len_cyrilic = 0
    len_spchars = len(filter(lambda x: x in sp_chars, text))
    return {
        'len': len_text,
        'digits': len_digits,
        'latin': len_latin,
        'cyrilic': len_cyrilic,
        'sp_chars': len_spchars,
    }


def too_few_cyrilic(s, coeff_latin=2.0, coeff_non_cyr=2.0):
    counts = char_types_count(s)
    if counts['latin'] * coeff_latin > counts['cyrilic']:
        return True
    return (counts['latin'] + counts['sp_chars'] + counts['digits']) * coeff_non_cyr > counts['cyrilic']


def too_many_numbers_spchars(s):
    counts = char_types_count(s)
    if (counts['digits'] + counts['sp_chars']) > (counts['latin'] + counts['cyrilic']):
        return True

    return False


def sanitize_lines(lines, cyrilic=True):
    lines = [l.strip() for l in lines]

    if cyrilic:
        lines = [l for l in lines if not too_few_cyrilic(l)]

    lines = [l for l in lines if not too_many_numbers_spchars(l)]

    lines = [l for l in lines if len(l) > 2 and len(l.split(' ')) >= 4]

    lines = [l for l in lines if stopwords_check_passed(l)]

    def bad_ending_check_passed(line):
        bad_endings = ['>>>', '...', '>>>.', ':']
        for be in bad_endings:
            if line[-len(be):] == be:
                return False
        return True

    lines = [l for l in lines if bad_ending_check_passed(l)]


    def starts_with_upper_check_passed(line):
        return line[0].isdigit() or (line[0].isupper() and not line[1].isupper())

    lines = [l for l in lines if starts_with_upper_check_passed(l)]

    lines = [l for l in lines if 'http://' not in l and 'https://' not in l]

    lines = uniq(lines)

    for i, line in enumerate(lines):
        # if not len(line):
        #     continue
        if line[-1] not in '.!?,;:':
            #print i, line[-1], line[-1] not in '.!?,;:'
            lines[i] += '.'

    return lines


def sanitize_html(html):
    html = html.decode('utf-8')
    html = html.replace(u'ё', u'е').replace(u'Ё', u'е')

    html = html.replace('</div>', '. </div>')
    for i in range(1, 5):
        html = html.replace('</h%d>' % i, '. </h%d>' % i)
    html = html.replace('</p>', '. </p>')
    html = html.replace('</li>', '. </li>')

    text = strip_tags(html)

    text = text.replace("\n", '##LINEBREAK##')

    text = re.compile('##LINEBREAK##\s+').sub('##LINEBREAK##', text)

    text = re.compile('\s+##LINEBREAK##').sub('##LINEBREAK##', text)

    text = re.compile('##LINEBREAK##(##LINEBREAK##)+').sub('##LINEBREAK##', text)

    text = text.replace('##LINEBREAK##', "\n")

    text = text.replace("\n.", ".\n")
    text = text.replace("\n,", ", ")
    text = text.replace(",\n", ", ")
    text = text.replace(u"…", "...")

    text = text.replace("\r\n", ' ').replace("\n", ' ')

    text = re.compile(r'^\s+$').sub('', text)


    text = re.compile(r'\[\d+\]').sub('', text)

    text = text.replace(' .', '.')
    text = re.compile(r'[\.!\?,;:]+\.').sub('.', text)
    text = re.compile(r'^[\^\.]*\s+').sub('', text)

    text = re.compile(r'\s\s+').sub(' ', text)

    return text.encode('utf-8')



def get_text_lines(urls, cyrilic=True):
    text = ''
    for get_plaintext_fun in (plaintext_through_requester, mercury):
        with time_limit(60*10):  #sometimes freezes, strange bug
            try:
                pool = Pool(10)  #10 results in yandex xml
                text = pool.map(get_plaintext_fun, urls)
                # html = map(get_plaintext_fun, urls)
                pool.close()
                pool.join()
            except TimeoutException:
                text = []
            except KeyError as e:
                print e  #strange bug
                text = []

        text = "\n".join(text)
        text = text.encode('utf-8')

        text = "\n".join(sanitize_lines(sentences(text.decode('utf-8')), cyrilic=cyrilic)).encode('utf-8')

        if len(text):
            break

    sents = []
    for sent in text.split("\n"):
        sents += sentences(sent)

    return sents



def spider(plain_text_file, queries=None, urls=None, related_queries_count=5, google_results_count=64, only_presearched=False):
    from parsers.search import google
    from parsers.wordstat import get_keywords

    def _get_urls(queries, related_queries_count, google_results_count, only_presearched):
        cache_urls_file = '/tmp/cache_urls_%s_%d_%d.json' % (hash(''.join(queries)), related_queries_count, google_results_count)
        if os.path.exists(cache_urls_file):
            with open(cache_urls_file) as f:
                urls = json.load(f)
            return urls

        urls = []

        if len(queries) == 1 and related_queries_count:
            query = queries[0]
            related_queries = [kw for kw, count in get_keywords(query)['more_with_this_keyword'].items()]
            related_queries = related_queries[:related_queries_count]
        else:
            related_queries = []

        queries = uniq(map(lambda q: q.strip(), queries + related_queries))

        for q in queries:
            # print q, 'unicode:', isinstance(q, unicode)
            search_res = google(q, results_count=google_results_count, only_presearched=only_presearched)

            print 'google results len', len(search_res), 'for query'
            pr(q)

            urls += search_res

        urls = [url for url in urls if not re.match(r'^.*\.pdf$', url.strip())]  #remove .pdf urls

        urls = uniq(urls)
        print 'len(urls)=', len(urls)

        with open(cache_urls_file, 'w+') as f:
            json.dump(urls, f)

        return urls

    if queries:
        urls = _get_urls(queries, related_queries_count, google_results_count, only_presearched=only_presearched)
        print 'len(urls):', len(urls)
    else:
        return None


    open(plain_text_file, 'w+').close()  #create empty file

    pm = ProgressMeter(unit='chunk', total=int(floor(len(urls) / 500)), rate_refresh=60)

    for urls_chunk in chunks(urls, 500):
        print '-'*30
        print 'NEXT CHUNK (%d)' % 500
        print '-'*30

        text_lines = get_text_lines(urls_chunk)

        text_lines = filter(lambda line: len(line) > 5, text_lines)
        text_lines = map(strip_remaining_tags, text_lines)

        with open(plain_text_file, 'a') as f:
            f.write("\n".join(text_lines))

        pm.update(1)

    print 'done get_text_lines'

    sentencize_file(plain_text_file)

    count_lines = int(subprocess.Popen(["wc", "-l", plain_text_file], stdout=subprocess.PIPE).communicate()[0].split(' ')[0])

    print 'count_lines:', count_lines

    if count_lines < 20:
        print 'TEXT IS TOO SHORT!!!'

    return count_lines




def sentencize_file(text_file):
    with open(text_file) as f:
        rawlines = f.readlines()

    def _decode_line(line):
        try:
            return line.strip().decode('utf-8')
        except UnicodeDecodeError as e:
            print e
            return ''

    rawlines = map(_decode_line, rawlines)

    sents = []
    for rawline in rawlines:
        sents += sentences(rawline)

    sents = sanitize_lines(sents)

    sents = map(lambda s: s.encode('utf-8'), sents)
    with open(text_file, 'w+') as f:
        f.write("\n".join(sents))

    os.system('sort --unique --random-sort %s | sponge %s' % (text_file, text_file))



"""
#deprecated

def strip_tags_deprecated(html):
    result = []
    parser = HTMLParser()
    parser.handle_data = result.append
    parser.feed(html)
    parser.close()
    return "\n ".join(result)


def generate(query, sent_count=20):
    plain_text_file = '%s/tmp/texts/%s.txt' % (PROJECT_PATH, slugify(query))

    if not os.path.exists(plain_text_file):
        spider(plain_text_file, queries=[query])

    return _generate(plain_text_file, sent_count)



def _generate(plain_text_file, sent_count, check_duplicate_lines=True):
    from lib.progress_meter import ProgressMeter
    pm = ProgressMeter(total=sent_count, unit='sentences', ticks=25, rate_refresh=10)

    #print char_types_count(' '.join(text.split("\n")).decode('utf-8'))

    Markov = Markov3(plain_text_file)
    generated_text = []


    def bad_size(s):
        size = len(s.split(' '))
        return size < 4 or size >= 15

    if check_duplicate_lines:
        duplicate = lambda line, text: line in text
    else:
        duplicate = lambda x, y: False

    pm.start()

    while len(generated_text) < sent_count:
        sent = Markov.generate_sentence()

        try:
            with time_limit(20):
                while bad_size(sent) or too_few_cyrilic(sent) or not stopwords_check(sent) or duplicate(sent, generated_text):
                    sent = Markov.generate_sentence()
        except TimeoutException as e:
            print e
            break

        generated_text += [sent]
        pm.update(1)

    return generated_text

"""