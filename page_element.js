var page = require('webpage').create();
var system = require('system');
//var process = require("child_process");
//var spawn = process.spawn;
//var execFile = process.execFile;





/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 */
function waitFor(testFx, onReady, timeOutMillis, name, onFailCallback) {
    if (typeof name != 'undefined') {
        console.log("'waitFor("+(name ? name : '')+")' binded");
    }
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 3000, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor("+(name ? name : '')+")' timeout");
                    if (onFailCallback) {
                        onFailCallback();
                    }
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor("+(name ? name : '')+")' finished in " + (new Date().getTime() - start) + "ms.");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 250); //< repeat check every 250ms
}

function logError() {
    console.log('');
    console.log('ERROR');
    console.log('');
    console.log(page.evaluate(function(){
        //return document.getElementsByClassName('is_video')[0].src;
        return document.body.innerHTML;
    }));
    console.log('');
    phantom.exit();
}

var useragents = [
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
];
page.settings.userAgent = useragents[Math.floor(useragents.length * Math.random())];
page.onInitialized = function () {
    page.evaluate(function () {

        //maybe support html5
        //https://stackoverflow.com/questions/30146353/phantomjs-not-mimicking-browser-behavior-when-looking-at-youtube-videos
        var create = document.createElement;
        document.createElement = function (tag) {
            if (tag === "video" || tag === 'VIDEO' || tag === 'Video') {
                var elem = create.call(document, 'div');
                elem.classList.add("is_video");
                elem.canPlayType = function () { return "probably" };
            } else if (tag === "source" || tag === 'SOURCE' || tag === 'Source') {
                var elem = create.call(document, 'div');
                elem.classList.add("is_source");
            } else {
                var elem = create.call(document, tag);
            }
            return elem;
        };

        //maybe support flash
        /*
        window.navigator = {
          plugins: { "Shockwave Flash": { description: "Shockwave Flash 11.2 e202" } },
          mimeTypes: { "application/x-shockwave-flash": { enabledPlugin: true } }
        };
         */

        var useragents = [
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
        ];
        window.navigator = {
            appCodeName: "Mozilla",
            appName: "Netscape",
            appVersion: useragents[Math.floor(useragents.length * Math.random())],
            cookieEnabled: true,
            doNotTrack: "1",
            hardwareConcurrency: 4,
            language: "en-US",
            languages: {
                0: "ru",
                1: "en-US",
                2: "en",
                3: "uk"
            },
            maxTouchPoints: 0,
            onLine: true,
            platform: "Linux x86_64",
            product: "Gecko",
            productSub: "20030107",
            userAgent: useragents[Math.floor(useragents.length * Math.random())],
            vendor: "Google Inc."
        };
    });
};



var url = system.args[1];
var css_path = system.args[2].replace(/^"(.*)"$/, '$1');
var attribute = system.args[3];
var valueValidation = typeof system.args[4] != 'undefined' ? system.args[4] : null;

function getValue(css_path, attribute, valueValidation) {
    css_path = css_path.split(' ').map(function(s){
        if (s == 'video') {
            return 'video,div.is_video';
        } else if (s == 'source') {
            return 'source,div.is_source';
        } else {
            return s;
        }
    }).join(' ');

    if (attribute == 'html') {
        var val = $(css_path).html();
    } else if (typeof $(css_path)[0] != 'undefined' && typeof $(css_path)[0][attribute] != 'undefined') {
        var val = $(css_path)[0][attribute];
    } else {
        var val = $(css_path).attr(attribute);
    }
    if (valueValidation && val) {
        var reg = new RegExp(valueValidation);
        if (!val.match(valueValidation)) {
            return false;
        }
    }
    return val;
}


function pageReady() {
    waitFor(function() {
        return page.evaluate(function(css_path, attribute, valueValidation, getValue) {
            if (typeof jQuery == 'undefined' || !jQuery.isReady) {return false;}
            return getValue(css_path, attribute, valueValidation);
//            return true;
        }, css_path, attribute, valueValidation, getValue);
    }, function(){
        var val = page.evaluate(function(css_path, attribute, valueValidation, getValue) {
            return getValue(css_path, attribute, valueValidation);
        }, css_path, attribute, valueValidation, getValue);
        console.log('VALUE:<![CDATA['+val+']]>');
        phantom.exit();
    }, 5000, 'pageReady', logError);
}


page.open(url, function (status) {
    if (status == 'success') {
        page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js", pageReady);
    } else {
        console.log('STATUS IS '+status);
        phantom.exit();
    }
});