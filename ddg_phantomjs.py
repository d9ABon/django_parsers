#!/usr/bin/env python


import requests
from requests.exceptions import Timeout, ConnectionError
import urllib
import re
import json
import os
import random
import subprocess
from time import sleep
#import lxml.html as lh

from lib.base import sentences, urlencode
from lib.decorators import retry_on_exception, retry_on_empty_result, time_limit, TimeoutException, block_printing_wrp




import logging, coloredlogs
coloredlogs.install()
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from requests.exceptions import ConnectionError as ReqConnectionError, ChunkedEncodingError
from http_request_randomizer.requests.proxy.requestProxy import RequestProxy
logging.getLogger("http_request_randomizer").setLevel(logging.WARNING)
conn_exceptions = (ConnectionError, ConnectionResetError, ReqConnectionError, ChunkedEncodingError)



USE_REQUESTER = False

ERROR_MSG = 'If this error persists'


class DDG_Not_Found(Exception):
    pass


class DDG_Error(Exception):
    pass


class DDG_RPS_Limit(Exception):
    pass


class DDG(object):

    cache = None
    phantomjs_path = None
    req_proxy = None
    proxies = []
    current_proxy = None
    phjs_images_js_path = None


    def __init__(self, phantomjs_path, cache=None):
        self.cache = cache
        self.phantomjs_path = phantomjs_path
        self.phjs_images_js_path = os.path.join(
            os.path.dirname(os.path.abspath(__file__)),
            'ddg_images.js',
        )

        self.get_new_proxies()
        self.randomize_proxy()


    def get_new_proxies(self):
        self.req_proxy = RequestProxy(sustain=True)
        self.req_proxy.set_logger_level(logging.WARNING)
        # self.req_proxy.logger.removeHandler(self.req_proxy.logger.handlers[-1])

        self.proxies = [(p.get_address(), [pr.name for pr in p.protocols])
                        for p in self.req_proxy.get_proxy_list()]

        #seems like phantomjs only allows HTTP and SOCKS5
        self.proxies = [p[0] for p in self.proxies if 'HTTP' in p[1]
                                                    or 'SOCS5' in p[1]]

        random.shuffle(self.proxies)


    def randomize_proxy(self):
        if not len(self.proxies):
            self.get_new_proxies()
        self.current_proxy = self.proxies.pop()

        logger.info('using proxy {}'.format(self.current_proxy))



    def images(self, query):
        data = None

        while data is None:
            try:
                cmd = [self.phantomjs_path,
                       '--proxy='+self.current_proxy,
                       self.phjs_images_js_path,
                       query]


                with time_limit(16):
                    result = subprocess.run(cmd, stdout=subprocess.PIPE)
                    out = result.stdout

                    #logger.info(result.stdout);return

                    if isinstance(out, bytes):
                        out = out.decode()

                    if "DDG.duckbar.failed('images')" in out:
                        raise DDG_Not_Found(query)

                    try:
                        data = json.loads(out)
                    except json.JSONDecodeError as e:
                        logger.error(e)
                        logger.info(out)
                        self.randomize_proxy()
                        data = None
                        continue

            except DDG_Not_Found as e:
                logger.info(e)
                raise e
            except Exception as e:
                logger.info(e)
                self.randomize_proxy()
                data = None
                continue

        data['snippets_sentences'] = []
        for i, snippet in enumerate(data['snippets']):
            data['snippets_sentences'] += sentences(snippet)



        return data


"""
#DEPRECATED:


try:
    from django.core.cache import cache
except ImportError:
    try:
        import pylibmc
        mc_servers = os.environ.get('MEMCACHIER_SERVERS', '127.0.0.1:11211').split(',')
        mc_user = os.environ.get('MEMCACHIER_USERNAME')
        mc_pass = os.environ.get('MEMCACHIER_PASSWORD')
        cache = pylibmc.Client(servers=mc_servers, username=mc_user, password=mc_pass, binary=True)
    except ImportError:
        cache = None


try:
    from parsers.pageparse import get_document_tree
    from parsers.plaintext import starts_with_upper, sanitize_lines
except ImportError as e:
    print('duckduckgo: some functions will not work - parsers required')






#@retry_on_exception(DDG_Error, retries=5, interval=1, exponent=1.5)
#@retry_on_exception(DDG_RPS_Limit)
@retry_on_exception(conn_exceptions, interval=15, raise_on_fail=False)
def request(self, url):
    request = None
    #with block_printing_wrp():
    while request is None:
        try:
            with time_limit(240):
                request = self.req_proxy.generate_proxied_request(url)
                if request is not None and request.status_code == 418:
                    raise DDG_RPS_Limit(request.content)
        except DDG_RPS_Limit as e:
            logger.info(e)
            self.req_proxy.randomize_proxy()
            request = None
        except Exception as e:
            logger.info(e)
            self.get_new_proxies()
            request = None

    #request = requests.get(url)

    return request


def images(self, query, logger=None):
    cache_key = 'DDG_images_%s' % hash(query)

    if self.cache:
        images = self.cache.get(cache_key)
        if images:
            return images

    url = 'http://duckduckgo.com/?q=%s' % urlencode(query)
    if logger:
        logger.info(url)

    html = self.request(url).text

    try:
        vqd = re.search(r'''vqd=[^\d-]?([\d-]+)''', html, flags=re.I|re.M|re.S).groups()[0]
    except AttributeError as e:
        with open('/tmp/ddg.html', 'w+') as f:
            f.write(html)
        logger.error('html is written to /tmp/ddg.html')
        raise

    url = 'https://duckduckgo.com/i.js?q={}&vqd={}&sp=0&iaf=size%3Aimagesize-large'.format(urlencode(query), vqd)
    if logger:
        logger.info(url)

    response = self.request(url)

    try:
        images = response.json()['results']
    except Exception:
        if logger:
            logger.info(response.content)
        raise DDG_Not_Found
    else:
        if self.cache:
            self.cache.set(cache_key, images, 60*60*24*30)

    return images







@retry_on_exception(DDG_Error, retries=5, interval=1, exponent=1.5)
@retry_on_exception(DDG_RPS_Limit)
def request(self, url):
    response = requests.get(url)
    if response.status_code == 418:
        raise DDG_RPS_Limit(response.content)
    return response


    #@deprecated
    #needs rewriting
    @retry_on_exception(DDG_Error, retries=10, interval=1, exponent=1.5, raise_on_fail=False)
    @retry_on_exception((Timeout, ConnectionError), retries=4, interval=5, raise_on_fail=False)
    def search(self, query):
        if isinstance(query, unicode):
            query = query.encode('utf-8')
        url = 'http://duckduckgo.com/html/?q=%s' % urllib.quote_plus(query)

        if USE_REQUESTER:
            url = "%srequest/?url=%s" % (random.choice(self.requesters), urllib.quote(url))

        html = requests.get(url, timeout=40).content

        if ERROR_MSG in html:
            raise DDG_Error(html)

        doc = get_document_tree(html)

        result_nodes = doc.cssselect('.result .result__snippet')
        ret_res = []
        for result_node in result_nodes:
            href = result_node.attrib['href']
            href = urllib.unquote(re.search(r'uddg=([^&]+)', href).groups()[0])
            if 'y.js?u3=' in href:
                href = urllib.unquote(re.search(r'u3=([^&]+)', href).groups()[0])
            snippet = result_node.text_content()
            ret_res.append({
                'url': href,
                'snippet': snippet,
            })

        return ret_res


    @staticmethod
    def get_sents_from_snippet(snippet, cyrilic=False):
        #print '-'*50
        #print snippet
        phrases = [p.strip() for p in snippet.split('...') if len(p.strip())]
        #print '---', phrases
        phrases = [p for p in phrases if p[-1] in '.?!;']
        #print '111', phrases
        phrases = [p for p in phrases if starts_with_upper(p)]
        phrases = [re.sub(r'(.*);$', r'\1.', p) for p in phrases]  #replace ending ; with dot
        #print '222', phrases
        phrases = sentences(' '.join(phrases))
        phrases = [p.strip() for p in phrases if len(p.strip())]
        #print '###', phrases
        #print '&&&', sanitize_lines(phrases, cyrilic=cyrilic)
        #import ipdb;ipdb.set_trace()

        return phrases





"""