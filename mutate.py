# -*- coding: utf8 -*-

import os
import pickle
import simplejson as json
import copy
import collections
import re
from hashlib import md5
from random import uniform
from collections import defaultdict

from settings import PROJECT_PATH
from lib.progress_meter import ProgressMeter
from lib.base import cols, split_to_cols, uniq
from lib.translate import *


with open(os.path.join(PROJECT_PATH, 'parsers', 'html_tags.txt')) as f:
    html_tags = f.readlines()
    html_tags = map(lambda line: line.decode('utf-8').lower().strip(), html_tags)




class Markov(object):

    alphabet = re.compile(u'[a-zA-Zа-яА-Я0-9-]+|[.,:;?!]+')

    dictionary = {}

    def __init__(self, file_plain, use_cache=True):
        self.file_plain = file_plain
        if use_cache:
            try:
                self.dictionary = pickle.load(open(self.__class__.get_dictionary_name(self.file_plain), "rb"))
            except (IOError, EOFError):
                self.dictionary = self.train(self.file_plain)
                pickle.dump(self.dictionary, open(self.__class__.get_dictionary_name(self.file_plain), "wb"))
        else:
            self.dictionary = self.train(self.file_plain)


    @classmethod
    def get_dictionary_name(cls, corpus):
        return "%s/tmp/markov/markov_%s_dictionary_%s" % (
            PROJECT_PATH,
            cls.__name__,  #to distinguish between markov degree
            md5(corpus).hexdigest()
        )


    def gen_lines(self, corpus):
        data = open(corpus)
        for line in data:
            line = line.decode('utf-8').lower().strip()
            if len(line) == 0:
                continue
            try:
                if line[len(line) - 1] not in '.!?':
                    line += '.'
            except Exception:
                import ipdb;ipdb.set_trace()
                pass
            yield line


    def gen_tokens(self, lines):
        for line in lines:
            for token in self.alphabet.findall(line):
                if token not in html_tags:
                    yield token


    def unirand(self, seq):
        sum_, freq_ = 0, 0
        for item, freq in seq:
            sum_ += freq
        rnd = uniform(0, sum_)
        for token, freq in seq:
            freq_ += freq
            if rnd < freq_:
                return token





class Markov3(Markov):

    def gen_trigrams(self, tokens):
        t0, t1 = '$', '$'
        for t2 in tokens:
            yield t0, t1, t2
            if t2 in '.!?':
                yield t1, t2, '$'
                yield t2, '$','$'
                t0, t1 = '$', '$'
            else:
                t0, t1 = t1, t2


    def train(self, corpus):
        lines = self.gen_lines(corpus)
        tokens = self.gen_tokens(lines)
        trigrams = self.gen_trigrams(tokens)

        bi, tri = defaultdict(lambda: 0.0), defaultdict(lambda: 0.0)

        for t0, t1, t2 in trigrams:
            bi[t0, t1] += 1
            tri[t0, t1, t2] += 1

        dictionary = {}
        for (t0, t1, t2), freq in tri.iteritems():
            if (t0, t1) in dictionary:
                dictionary[t0, t1].append((t2, freq / bi[t0, t1]))
            else:
                dictionary[t0, t1] = [(t2, freq / bi[t0, t1])]

        return dictionary

    def generate_sentence(self):
        phrase = ''
        t0, t1 = '$', '$'
        while 1:
            try:
                t0, t1 = t1, self.unirand(self.dictionary[t0, t1])
            except KeyError:
                #TODO investigate bug
                return ''
                #import ipdb; ipdb.set_trace()
            if t1 == '$':
                break
            if t1 in '.!?,;:' or t0 == '$':
                phrase += t1
            else:
                phrase += ' ' + t1
        return phrase.capitalize()




class Markov4(Markov):

    def gen_quadrams(self, tokens):
        t0, t1, t2 = '$', '$', '$'
        for t3 in tokens:
            yield t0, t1, t2, t3
            if t3 in '.!?':
                yield t1, t2, t3, '$'
                yield t2, t3, '$', '$'
                yield t3, '$','$', '$'
                t0, t1, t2 = '$', '$', '$'
            else:
                t0, t1, t2 = t1, t2, t3


    def train(self, corpus):
        lines = self.gen_lines(corpus)
        tokens = self.gen_tokens(lines)
        quadgrams = self.gen_quadrams(tokens)

        # from lib.base import pr
        # pr(list(quadgrams)[:20])
        # raise Exception

        bi, tri, quad = defaultdict(lambda: 0.0), defaultdict(lambda: 0.0), defaultdict(lambda: 0.0)

        for t0, t1, t2, t3 in quadgrams:
            bi[t0, t1] += 1
            tri[t0, t1, t2] += 1
            quad[t0, t1, t2, t3] += 1

        dictionary = {}
        for (t0, t1, t2, t3), freq in quad.iteritems():
            if (t0, t1, t2) in dictionary:
                dictionary[t0, t1, t2].append((t3, freq / tri[t0, t1, t2]))
            else:
                dictionary[t0, t1, t2] = [(t3, freq / tri[t0, t1, t2])]

        return dictionary

    def generate_sentence(self):
        phrase = ''
        t0, t1, t2 = '$', '$', '$'
        while 1:
            try:
                #t0, t1 = t1, self.unirand(self.dictionary[t0, t1])
                t0, t1, t2 = t1, t2, self.unirand(self.dictionary[t0, t1, t2])
            except KeyError:
                #TODO investigate bug
                return ''
                #import ipdb; ipdb.set_trace()
            if t2 == '$':
                break
            if t2 in '.!?,;:' or t1 == '$':
                phrase += t2
            else:
                phrase += ' ' + t2
        return phrase.capitalize()



















class Denormalize(object):
    text = []
    max_length = None
    variants = [[]]
    prev_variants = [[]]

    def __init__(self, text, max_length=7):
        self.text = text.split(' ')
        self.max_length = max_length


    @staticmethod
    def add_word_to_all_variants(lst, word_str):
        for i, elem in enumerate(lst):
            lst[i] += [word_str]
        return lst


    @staticmethod
    def remove_first_word_from_all_variants(lst):
        first_word = []
        for i in range(len(lst)):
            first_word += [lst[i][0]]
            del lst[i][0]

        if len(uniq(first_word)) > 1:
            #unique variants
            lst_out = []
            frozen_lst = [frozenset(variant) for variant in lst]
            frozen_lst_un = uniq(frozen_lst)
            for i, variant in enumerate(frozen_lst):
                if variant in frozen_lst_un:
                    lst_out += [lst[i]]
            lst = lst_out

        return lst


    @staticmethod
    def deepcopy(lst):
        ret = []
        for i in range(len(lst)):
            ret += [[]]
            for word in lst[i]:
                ret[i] += [word]
        return ret


    @staticmethod
    def unique_by_lowercase(word_objs_list):
        words_by_loweredstr = collections.defaultdict(list)
        for i, word in enumerate(word_objs_list):
            #will be like {'loweredword': [wobj1, wobj2], ...}
            words_by_loweredstr[word.word.lower()] += [word]

        ret_lst = []
        for words in words_by_loweredstr.values():
            ret_lst += [random.choice(words)]

        return ret_lst


    def add_word(self, word):
        if len(self.text) == self.max_length:
            self.variants = Denormalize.remove_first_word_from_all_variants(Denormalize.deepcopy(self.variants))
            del self.text[0]
        self.text += [word]
        self.next_variant(word)


    def all_variants(self):
        for nw_str in self.text:
            self.next_variant(nw_str)

    def next_variant(self, nw_str):
        try:
            nw = NormWord.objects.get(word=nw_str)
        except NormWord.DoesNotExist:
            print 'NormWord.DoesNotExist'
            return False

        self.prev_variants = Denormalize.deepcopy(self.variants)

        words = Word.objects.filter(norm_word=nw)
        words = Denormalize.unique_by_lowercase(words)
        for i, word in enumerate(words[:2]):
            if i == 0:
                self.variants = Denormalize.add_word_to_all_variants(Denormalize.deepcopy(self.variants), word)
            else:
                self.variants += Denormalize.add_word_to_all_variants(Denormalize.deepcopy(self.prev_variants), word)


    def format_variants(self):
        ret = []
        for variant in self.variants:
            ret += [(
                ' '.join([w.word for w in variant]),
                ' '.join([w.morfology for w in variant]),
            )]
        return ret



"""




class Markov1(object):

    file_plain = '%s/tmp/mutate_titles_plain.txt' % PROJECT_PATH
    file_norm = '%s/tmp/mutate_titles_norm.txt' % PROJECT_PATH
    file_morf = '%s/tmp/mutate_morfology' % PROJECT_PATH

    dictionary_plain = {}
    dictionary_norm = {}


    def __init__(self):
        if not os.path.exists(self.file_plain) or not os.path.exists(self.file_norm) or not os.path.exists(self.file_morf):
            export_titles_to_text(self.file_plain, self.file_norm, self.file_morf)

        self.dictionary_plain = self.make_dict(self.file_plain)
        self.dictionary_norm = self.make_dict(self.file_norm)


    def make_dict(self, file_in):
        try:
            dictionary = json.load(open("%s/tmp/markov/markov_dictionary_%s" % (PROJECT_PATH, hash(file_in)), "rb"))
            return dictionary
        except IOError:
            dictionary = {}

        f = open(file_in, 'r')
        lines = f.readlines()
        f.close()

        #Превращаем текст в одну строку
        #s = ' '.join(lines)
        #Выделяем все слова из строки (выражение в кавычках или в скобках считается одним словом)
        for line in lines:
            words = []
            groups = re.findall("((\"[^\"]+\")|(\([^\)]+\))|([^\(\)\"'\s]+))(\s+|\z)", line)
            for group in groups:
                words += [group[0].decode('utf8')]
                #print cols(words)
            # import ipdb;ipdb.set_trace()

            count = len(words)
            prev_word = None
            for i in range(count):
                if i > 0:
                    #if words[i] not in dictionary[prev_word]:
                    dictionary[prev_word] += [words[i]]

                prev_word = words[i]

                if i < count - 1 and prev_word not in dictionary:
                    dictionary[prev_word] = []

        # for k,v in dictionary.items():
        #     print k, '------'
        #     print cols(v)
        json.dump(dictionary, open("%s/tmp/markov/markov_dictionary_%s" % (PROJECT_PATH, hash(file_in)), "wb"))

        return dictionary


    #Функция для генерации текста. $count – количество слов, которое будем генерировать
    def markov(self, dictionary):
        words = dictionary.keys()
        word = random.choice(words)
        #text = []
        # for i in range(count):
        while True:
            #text += [word]
            yield word
            #Следующее слово - случайное слово из тех, что идут в исходном тексте за текущим словом
            # print '-------------------'
            # print word
            # print ', '.join([w.encode('utf-8') for w in dictionary[word]])
            try:
                word = random.choice(dictionary[word])
            except KeyError:
                yield None
                word = random.choice(words)


        #return ' '.join(text)

    def markov2(self):
        words = self.dictionary_norm.keys()
        word = random.choice(words)
        while True:
            yield word

            nw = NormWord.objects.get(word=word)
            wrds1 = list(Word.objects.filter(norm_word=nw))
            wrds2 = []
            for nw_str in self.dictionary_norm[word]:
                nw = NormWord.objects.get(word=nw_str)
                wrds2 += list(Word.objects.filter(norm_word=nw))

            tmpdict = collections.defaultdict(list)

            try:
                for w1 in wrds1:
                    for w2 in wrds2:
                        #print w1, w2
                        if w2.word in self.dictionary_plain[w1.word]:
                            tmpdict[w1.word] += [w2.word]
            except Exception as e:
                print e
                import ipdb;ipdb.set_trace()
                pass
            # print wrds1
            # print wrds2
            for k,v in tmpdict.items():
                print k
                print ', '.join([vv.encode('utf-8') for vv in v])
            print '--------------'
            print cols(self.dictionary_norm[word])

            new_word = random.choice(self.dictionary_norm[word])
            word = new_word
"""






