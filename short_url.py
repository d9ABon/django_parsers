import subprocess
import shlex
import urllib
import urllib2
import simplejson as json

def run(cmd):
    out = subprocess.Popen(shlex.split(cmd.encode('utf-8')), stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    return out

SHORTEN_API_KEY = '8bc0c0d9a0a994242b1c0836c8e38520'

def shorten(url):
    headers = {'public-api-token': SHORTEN_API_KEY,}
    data = {'urlToShorten': url,}
    data = urllib.urlencode(data)
    req = urllib2.Request('https://api.shorte.st/v1/data/url', data, headers)
    req.get_method = lambda: 'PUT'
    resp = urllib2.urlopen(req)

    data = resp.read()

    return json.loads(data)['shortenedUrl']

    cmd = """curl -H "public-api-token: %(SHORTEN_API_KEY)s" -X PUT -d "urlToShorten=%(url)s" https://api.shorte.st/v1/data/url""" % {
        'url': url,
        'SHORTEN_API_KEY': SHORTEN_API_KEY,
    }
    out, errors = run(cmd)
    print out, errors