import requests
import json
import re
import sys
import time
import random
import datetime
from lib.base import pr, urldecode, urlencode, instapush
from parsers.plaintext import sanitize_lines

"""
GET https://api.cognitive.microsoft.com/bing/v5.0/search?q=bill gates&count=10&offset=0&mkt=en-us&safesearch=Moderate HTTP/1.1
Host: api.cognitive.microsoft.com
Ocp-Apim-Subscription-Key: ae431138cc534f3996016123bdf92e08
"""

class BingRateLimitExceeded(Exception):
    pass

class BingSearch(object):

    keys = []

    def __init__(self, conf):
        keys = conf['keys']
        random.shuffle(keys)

        for key_conf in keys:
            vu = key_conf['valid_until']
            if not isinstance(vu, datetime.date):
                vu = datetime.datetime.strptime(vu, '%Y-%m-%d')
            if vu > datetime.datetime.now().date():
                self.keys.append(key_conf['key'])



    def _search(self, query):
        url = "https://api.cognitive.microsoft.com/bing/v5.0/search"
        url += "?q=%s" % urlencode(query)
        url += "&count=10&offset=0&mkt=en-us&safesearch=Moderate"
        headers = {
            'Host': 'api.cognitive.microsoft.com',
            'Ocp-Apim-Subscription-Key': self.keys[0],
        }

        data = requests.get(url, headers=headers).json()

        if 'statusCode' in data:
            if int(data['statusCode']) in (403, 429):
                raise BingRateLimitExceeded("%d %s" % (data['statusCode'], data['message']))
            else:
                raise Exception("Bing: %d %s" % (data['statusCode'], data['message']))

        def _snippet(snippet):
            snippet = snippet.split('...')
            snippet = sanitize_lines(snippet, cyrilic=False)
            return ' '.join(snippet)

        data = data['webPages']['value']
        data = [{
            'name': item['name'],
            'url': urldecode(re.search(r'&r=([^&]+)', item['url']).groups()[0]),
            'snippet': _snippet(item['snippet']),
        } for item in data]

        return data


    def search(self, query):
        while True:
            if not self.keys:
                instapush('all bing keys expired')
                raise Exception('bing key expired')

            result = []

            try:
                result = list(self._search(query))
            except BingRateLimitExceeded:
                print "expired", self.keys[0]  #STUB
                del self.keys[0]
                continue
            except Exception as e:
                print e
                return []
            else:
                return result

            return result



