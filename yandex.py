# -*- coding: utf8 -*-

import os
import datetime
import requests
import time
import sys
import base64
import xml.etree.ElementTree as ET

from lib.base import urlencode


class YandexRateLimitExceeded(Exception):
    pass
class YandexNoResults(Exception):
    pass
class YandexRpsLimit(Exception):
    pass


def yandex_request(yandex_url, proxy_through_api=None):
    if isinstance(yandex_url, unicode):
        yandex_url = yandex_url.encode('utf-8')
    if not proxy_through_api:
        xml = requests.get(yandex_url).content
    else:
        xml = requests.post(proxy_through_api, {
            'yandex_url_b64': base64.b64encode(yandex_url),
        }).content

    root = ET.fromstring(xml)
    response = root.find('response')

    error = response.find('error')
    if error is not None:
        if int(error.attrib['code']) == 32:
            raise YandexRateLimitExceeded(error.text)

        if 'code="32"' in xml:
            print 'legacy 2 YandexRateLimitExceeded'
            raise YandexRateLimitExceeded(error.text)

        if 'RPS' in xml:
            raise YandexRpsLimit(error.text)

        raise Exception(error.text)

    if 'code="32"' in xml:
        print 'legacy 1 YandexRateLimitExceeded'
        raise YandexRateLimitExceeded()

    #misspelled
    #if response.find('reask') is not None:
    #    print xml
    #    raise YandexNoResults(yandex_url)

    try:
        results = response.find('results')
        grouping = results.find('grouping')
    except Exception as e:
        print e
        print xml
        raise

    #print grouping.find('found-docs-human').text

    groups = grouping.findall('group')
    for group in groups:
        doc = group.find('doc')
        url = doc.find('url').text
        title = ''.join(doc.find('title').itertext())

        snippet = []
        headline = doc.find('headline')
        passages = doc.find('passages')
        if headline is not None:
            snippet += [''.join(headline.itertext())]
        if passages is not None:
            for passage in passages.findall('passage'):
                snippet += [''.join(passage.itertext())]
        snippet = ' '.join(snippet)

        lang = doc.find('properties').find('lang')
        if lang is not None:
            lang = lang.text
        else:
            lang = 'ru'


        yield {'url': url, 'title': title, 'snippet': snippet, 'lang': lang}


def yandex_xml_search(query, lang='ru', proxy_through_api=None, raiseLimitExceeded=False):
    yandex_url = os.environ['YANDEX_XML_URL']
    if lang == 'en':
        yandex_url = yandex_url.replace('yandex.ru', 'yandex.com')
        yandex_url += '&l10n=en'
    else:
        yandex_url = yandex_url.replace('yandex.com', 'yandex.ru')
        yandex_url += '&l10n=ru'
    yandex_url += '&sortby=rlv&filter=none'
    yandex_url += '&query=%s' % urlencode(query.encode('utf-8'))

    while True:
        result = []

        try:
            result = list(yandex_request(yandex_url, proxy_through_api))
        except YandexRateLimitExceeded:
            if not raiseLimitExceeded:
                print 'sleep...'
                sys.stdout.flush()
                time.sleep(60 * 15)
                continue
            else:
                raise
        except YandexNoResults:
            print 'no results'
            return []
        except YandexRpsLimit:
            print 'RPS limit exceeded, sleep 1 min...'
            sys.stdout.flush()
            time.sleep(60)
            continue
        except Exception as e:
            print e
            return []
        else:
            return result

        return result


#deprecated: it's only max limits, not remaining
def limits_info():
    from dateutil.parser import parse as strtotime
    yandex_url = os.environ['YANDEX_XML_URL'] + '&action=limits-info'
    xml = requests.get(yandex_url).content
    root = ET.fromstring(xml)
    response = root.find('response')

    error = response.find('error')
    if error is not None:
        raise Exception(error.text)

    limits = response.find('limits')
    tis = limits.findall('time-interval')

    res = {'total':0}
    for ti in tis:
        fr = tis[0].attrib['from']
        if strtotime(fr).day == datetime.datetime.now().day:
            pass

    import ipdb;ipdb.set_trace()

