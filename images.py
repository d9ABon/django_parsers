# -*- coding: utf8 -*-


import re


def extract_images(html):
    res = re.search('<img[^>]+src=["\']([^"\']*)["\'][^>]*>', html)
    if not res:
        return []

    return res.groups()