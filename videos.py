# -*- coding: utf-8 -*-
from base64 import b64encode
from hashlib import md5
import re
import os
import simplejson as json
from itertools import ifilterfalse

from django.core.cache import cache

from lib.base import urlencode
from lib.proxy import Proxy

from settings import PROJECT_PATH

def get_videos(query):
    query = urlencode(query.lower().encode('utf-8'))
    
    #cache in files
    search_type = 'videos'
    results_count = 20  #кол-во результатов за один раз (не более 50)
    cache_file = os.path.join(PROJECT_PATH, 'tmp', 'google', str('%.019s' % md5(query).hexdigest())[:3], '%s_%.019s_%d.json' % (search_type, md5(query).hexdigest(), results_count))
    if not os.path.exists(os.path.dirname(cache_file)):
        os.makedirs(os.path.dirname(cache_file))
    if os.path.exists(cache_file):
        print 'found new cache:', cache_file
        try:
            with open(cache_file) as f:
                results = json.load(f)
        except json.JSONDecodeError as e:  #strange bug
            os.unlink(cache_file)
            print query
        else:
            return results



    url='http://gdata.youtube.com/feeds/api/videos?q=%(query)s&format=5&lr=ru&max-results=%(results_count)d&v=2&alt=jsonc' % {
        'query': query,
        'results_count': results_count,
    }

    requestSuccess = False
    while not requestSuccess:
        res = Proxy().request(url, use_proxy=True)
        #$response = str_replace('default', 'idefault', $response);
        try:
            if '<!DOCTYPE html' in res:
                raise json.JSONDecodeError("<!DOCTYPE html in res", '', 0)

            items = json.loads(res)
            if items['data']['totalItems'] == 0:
                return []
            items = items['data']['items']
        except json.JSONDecodeError as e:
            requestSuccess = False
            #data = {'responseStatus': 403, 'responseDetails': str(e)}  #data is None
        except Exception as e:
            requestSuccess = False
            print e
            import ipdb;ipdb.set_trace()
            return []
        else:
            requestSuccess = True


        if not requestSuccess:
            print 'request fail!', res
            #import ipdb;ipdb.set_trace()
            Proxy.move_host_to_bottom(Proxy.choose_proxy_from_top())




    embed_allowed = lambda item: item['accessControl']['embed'] == 'allowed'
    items = filter(embed_allowed, items)

    items = [{
        'youtube_id': item['id'],
        'title': item['title'],
        'duration': item['duration'],
    } for item in items]


    # cache.set(cache_key, results, 60 * 60 * 24 * 365)
    with open(cache_file, 'w+') as f:
        json.dump(items, f)
        print 'writing:', cache_file

    return items




def get_videos_old(query):
    from lib.php import php

    if isinstance(query, unicode):
        query = query.encode('utf-8')
    cache_key = 'videos_by_query_%s' % md5(query).hexdigest()
    videos = cache.get(cache_key)
    if videos is not None:
        return videos

    php_vars = {'q': query, 'query': query}

    files = ['../seodor/conf.php', '../seodor/inc/functions.php',
             '../seodor/inc/mc.php', '../seodor/inc/parsers/videoYoutubeApi.php']
    videos = php("""videoYoutubeApi($query, 'ru')""", files, php_vars)

    print 'len videos:', len(videos)

    for key, video in enumerate(videos):
        title, description, thumb, link = video.split(' * ')
        v_id = re.search('\?v=([^&]+)&', link).groups()[0]
        videos[key] = {'title': title, 'description':description, 'thumb': thumb,
                       'link': link, 'id': v_id}


    if len(videos):
        cache.set(cache_key, videos, 60 * 60 * 24 * 7)

    #import ipdb;ipdb.set_trace()
    return videos


"""
function getVideos ($Method, $Numres, $videos = '', $chr) {
global $myimages, $isRezervuar, $folderDor, $bulldozer;

	if (!is_array($videos)) $videos = (array)$videos;
	$videos  = array_diff($videos, array(null));

	if (empty($videos)){
		global $cache, $cacheErrorVideo;

		$videos = rezervuarRead ('videos');

		if (!$cacheErrorVideo) $cache = 0; // записываем или не записываем в кеш

	} else {
		if (!$isRezervuar){
			if (!$_SESSION['rezervuarVideos']){
				rezervuarWrite ('videos', $videos);
				$_SESSION['rezervuarVideos'] = 1;
			}
		}
	}

	$videos  = rand_content($videos);

	foreach($videos as $id=>$str) {
		$emb_src[$id] = explode(' * ', $str);
	}

	if ($Numres == 1)$counts = count($emb_src);
	else $counts = $Numres;

	switch ($Method) {

		case 'URL_TRB':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]=='')break;
				if($myimages){//encode_str
					$contents[] = '/'.$folderDor.$myimages.'/'.encode_str($emb_src[$i][2]).'.jpg';
				} else {
					$contents[] = $emb_src[$i][2];
				}
			}
		break;

		case 'URL_TUBE':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][3]==''){break;}
				$contents[] = $emb_src[$i][3];
			}
		break;

		case 'ID_TUBE':
			for ($i=0; $i<$counts; $i++) {
				$url = $emb_src[$i][3];
				parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
				$embid =  $my_array_of_vars['v'];
				if ($embid[$i]==''){break;}
				$contents[] = $embid;
			}
		break;

		case 'EMB':
			for ($i=0; $i<$counts; $i++) {
				$url = $emb_src[$i][3];
				parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
				$embid =  $my_array_of_vars['v'];
				if ($embid[$i]==''){break;}
				//$contents[] = '<iframe width="600" height="500" src="http://www.youtube.com/embed/'.$embid.'" frameborder="0" allowfullscreen></iframe>';
				$contents[] = "<object width='600' height='500'>\n
				<param name='movie' value='http://www.youtube.com/v/$embid&autoplay=1&rel=0'/>\n
				<param name='wmode' value='transparent'/>\n
				<embed src='http://www.youtube.com/v/$embid&autoplay=1&rel=0' type='application/x-shockwave-flash' wmode='transparent' width='600' height='500'/>\n
				</object>";
			}
		break;

		case 'TITLE':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$contents[] = strip_tags ( $emb_src[$i][0] );
			}
		break;

		case 'CONTENT':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][1]==''){break;}
				$contents[] = replace_filter(strip_tags($emb_src[$i][1]));
			}
		break;

		case 'HREF':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$title = strip_tags(trim($emb_src[$i][0]));
				if ($bulldozer) $contents[] = '<a href="'.subKey($title).'" title="'.str_replace('"', '', $title).'">'.$title.'</a>';
				else $contents[] = $title;
			}
		break;

		case 'URL':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$title = strip_tags(trim($emb_src[$i][0]));
				$contents[] = subKey($title);
			}
		break;

		case 'HREF_TRB':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$title = str_replace('"', '', strip_tags(trim($emb_src[$i][0])));
				if ($bulldozer) {
					if($myimages){//encode_str
						$contents[] = '<a href="'.subKey($title).'" title="'.$title.'"><img src="/'.$folderDor.$myimages.'/'.encode_str($emb_src[$i][2]).'.jpg" alt="'.$title.'" width="120" /></a>';
					} else {
						$contents[] = '<a href="'.subKey($title).'" title="'.$title.'"><img src="'.$emb_src[$i][2].'" alt="'.$title.'" width="120" /></a>';
					}
				} else {
					if($myimages){//encode_str
						$contents[] = '<img src="/'.$folderDor.$myimages.'/'.encode_str($emb_src[$i][2]).'.jpg" alt="'.$title.'" width="120" />';
					} else {
						$contents[] = '<img src="'.$emb_src[$i][2].'" alt="'.$title.'" width="120" />';
					}
				}
			}
		break;

		case 'HREF_TUBE':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$title = str_replace('"', '', strip_tags(trim($emb_src[$i][0])));
				$contents[] = '<a href="'.$emb_src[$i][3].'" target="_blank" title="'.$title.'">'.$title.'</a>';
			}
		break;

		case 'HREF_TUBE_TRB':
			for ($i=0; $i<$counts; $i++) {
				if ($emb_src[$i][2]==''){break;}
				$title = str_replace('"', '', strip_tags(trim($emb_src[$i][0])));
				if($myimages){//encode_str
					$contents[] = '<a href="'.$emb_src[$i][3].'" target="_blank" title="'.$title.'"><img src="/'.$folderDor.$myimages.'/'.encode_str($emb_src[$i][2]).'.jpg" alt="'.$title.'" width="120" /></a>';
				} else {
					$contents[] = '<a href="'.$emb_src[$i][3].'" target="_blank" title="'.$title.'"><img src="'.$emb_src[$i][2].'" alt="'.$title.'" width="120" /></a>';
				}
			}
		break;
	}

	if ($Numres !==1) {
		$contents = implode_replace($contents, $chr);
	}

	return $contents;

}
"""