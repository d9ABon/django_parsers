def strip_tags_deprecated(html):
    result = []
    parser = HTMLParser()
    parser.handle_data = result.append
    parser.feed(html)
    parser.close()
    return "\n ".join(result)


def generate(query, sent_count=20):
    plain_text_file = '%s/tmp/texts/%s.txt' % (PROJECT_PATH, slugify(query))

    if not os.path.exists(plain_text_file):
        spider(plain_text_file, queries=[query])

    return _generate(plain_text_file, sent_count)



def _generate(plain_text_file, sent_count, check_duplicate_lines=True):
    from lib.progress_meter import ProgressMeter
    pm = ProgressMeter(total=sent_count, unit='sentences', ticks=25, rate_refresh=10)

    #print char_types_count(' '.join(text.split("\n")).decode('utf-8'))

    Markov = Markov3(plain_text_file)
    generated_text = []


    def bad_size(s):
        size = len(s.split(' '))
        return size < 4 or size >= 15

    if check_duplicate_lines:
        duplicate = lambda line, text: line in text
    else:
        duplicate = lambda x, y: False

    pm.start()

    while len(generated_text) < sent_count:
        sent = Markov.generate_sentence()

        try:
            with time_limit(20):
                while bad_size(sent) or too_few_cyrilic(sent) or not stopwords_check(sent) or duplicate(sent, generated_text):
                    sent = Markov.generate_sentence()
        except TimeoutException as e:
            print e
            break

        generated_text += [sent]
        pm.update(1)

    return generated_text