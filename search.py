# -*- coding: utf-8 -*-

"""
https://developers.google.com/custom-search/json-api/v1/overview
var cx = '007975494601334545805:ft9cabcnxmw';

http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=test
&start=4  searchType=image rsz=4 hl=ru
"""
import os
import json
import re
import time
import random
from hashlib import md5
from lib.proxy import Proxy
from lib.base import urlencode, slugify
from lib.decorators import profile_fun
from hashlib import md5
from django.core.cache import cache
from settings import PROJECT_PATH
from urllib import unquote_plus


@profile_fun
def google(query, results_count=64, search_type='web', additional_search_params='', use_proxy=True, proxy_inst=None, only_presearched=False):
    if isinstance(query, unicode):
        query = query.encode('utf-8')
    if not proxy_inst:
        proxy_inst = Proxy()

    def cache_filepath(query, search_type, results_count):
        return os.path.join(PROJECT_PATH, 'tmp', 'google', str('%.019s' % md5(query).hexdigest())[:3], '%s_%.019s_%d.json' % (search_type, md5(query).hexdigest(), results_count))

    for i in range(64, results_count-1, -8):  #try files with more results
        if os.path.exists(cache_filepath(query, search_type, i)):
            print 'found new cache:', cache_filepath(query, search_type, i)
            with open(cache_filepath(query, search_type, i)) as f:
                results = json.load(f)
            return results[:results_count]

    if only_presearched:
        return []

    encoded_query = urlencode(query)

    class ParseError(Exception):
        pass
    def _parse(html):
        try:
            results = re.findall(r'''<h3 class="r">\s*<a[^>]+href="([^"]+)"''', html, flags=re.MULTILINE | re.DOTALL | re.IGNORECASE)
            for i, res in enumerate(results):
                if res[:7] == '/url?q=':
                    results[i] = unquote_plus(res[7:])
        except Exception as e:
            print e
            raise ParseError()

        return results

    def _request(start=0):
        url = 'https://www.google.com/search?q=%(encoded_query)s&start=%(start)s&num=%(results_count)s&%(additional_search_params)s' % {
            'start': start,
            'encoded_query': encoded_query,
            'results_count': results_count,
            'additional_search_params': additional_search_params,
        }
        data = []
        while not data:
            res = proxy_inst.request(url, next_proxy_on=('CaptchaRedirect',))
            try:
                data = _parse(res)
                break
            except ParseError as e:
                data = []
                proxy_inst.proxies = proxy_inst.proxies[1:]
                continue

        return data

    results = _request()[:results_count]

    if len(results):
        if not os.path.exists(os.path.dirname(cache_filepath(query, search_type, results_count))):
            os.makedirs(os.path.dirname(cache_filepath(query, search_type, results_count)))
        with open(cache_filepath(query, search_type, results_count), 'w+') as f:
            json.dump(results, f)
            print 'writing:', cache_filepath(query, search_type, results_count)

    return results[:results_count]


def google_images(query, results_count=16):
    #contentNoFormatting width height unescapedUrl
    return google(query, results_count, search_type='images', additional_search_params='imgsz=large')



@profile_fun
def bing_related_queries(query, use_proxy=True):
    if isinstance(query, unicode):
        query = query.encode('utf-8')

    def cache_filepath(query):
        return os.path.join(PROJECT_PATH, 'tmp', 'bing', str('%.019s' % md5(query).hexdigest())[:3], '%s_%.019s.json' % ('related', md5(query).hexdigest()))
    if os.path.exists(cache_filepath(query)):
        print 'found new cache:', cache_filepath(query)
        if os.path.getsize(cache_filepath(query)) == 0:
            results = []
        else:
            with open(cache_filepath(query)) as f:
                results = json.load(f)
        return results



    url = 'http://api.bing.com/osjson.aspx?query=%s' % urlencode(query)
    res = Proxy().request(url, use_proxy=use_proxy)

    try:
        if '<!DOCTYPE html' in res:
            raise json.JSONDecodeError("<!DOCTYPE html in res", '', 0)

        results = json.loads(res)[1]
    except json.JSONDecodeError as e:
        results = []  #data is None







    if not os.path.exists(os.path.dirname(cache_filepath(query))):
        os.makedirs(os.path.dirname(cache_filepath(query)))
    with open(cache_filepath(query), 'w+') as f:
        if len(results):
            json.dump(results, f)
            print 'writing:', cache_filepath(query)

    return results


def yandex(query, username, key):
    #https://yandex.ru/search/xml?user=aa2syvxufhgq&key=03.155506513:30d1f80bac137f3025b983f27c815508&action=limits-info

    url = "https://yandex.ru/search/xml?user=%(username)s&key=%(key)s&lr=225&query=%(query)s"
