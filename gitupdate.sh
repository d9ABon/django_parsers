#!/bin/sh

CURRENT_DIR="$( cd "$( dirname "$0" )" && pwd )"

cd ${CURRENT_DIR}
git add .
git commit -m "gitupdate.sh" .
git pull origin master
git push origin master