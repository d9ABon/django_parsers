import string
import operator
import re
import os

try:
    from django.core.cache import cache
except ImportError:
    import pylibmc
    mc_servers = os.environ.get('MEMCACHIER_SERVERS', '127.0.0.1:11211').split(',')
    mc_user = os.environ.get('MEMCACHIER_USERNAME')
    mc_pass = os.environ.get('MEMCACHIER_PASSWORD')
    cache = pylibmc.Client(servers=mc_servers, username=mc_user, password=mc_pass, binary=True)



from lib.base import uniq, pr


def _normalize_to_words(text, morph, norm_words_blacklist):
    MIN_WORD_LEN = 4

    text = re.sub(r'[^\w\s]+', '', text, flags=re.S)
    words = re.split(r'\s+', text, flags=re.S)

    words = [w for w in words if len(w) >= MIN_WORD_LEN]

    words = [_get_norm(w, morph) for w in words]

    words = [w for w in words if w not in norm_words_blacklist]

    return words


def _get_norm(word, morph):
    norm_w = cache.get('morph.normalize_%s' % hash(word))
    if norm_w:
        return norm_w
    try:
        norm_w = morph.normalize(word.upper())
    except Exception as e:
        norm_w = word.upper()
    if isinstance(norm_w, set):
        norm_w = norm_w.pop()
    if not isinstance(norm_w, unicode):
        norm_w = norm_w.decode('utf-8')

    cache.set('morph.normalize_%s' % hash(word), norm_w, 60*60*24*7)
    return norm_w



def _frequency(query_words, morph, words=None, limit=10):
    table = string.maketrans("","")

    #translate cannot take unicode
    words = map(lambda w: w.encode('utf-8').translate(table, string.punctuation).decode('utf-8'), words)
    words = filter(len, words)

    frequencies = {}
    #for word in words:
    #    frequencies[word] = frequencies.get(word, 0) + 1

    summ_words = sum(frequencies.values())

    #make all possible kws combinations
    #ex. from keywords ['a','b','c','d'] ==> ['a b', 'b c', 'c d', 'a b c', 'b c d', 'a b c d']
    kws = query_words
    combinations = []
    for kw_len in (1, 2):  #(2,3,4)#min 2 words, max 4 words
        for i in range(len(kws) - kw_len + 1):
            combinations += [' '.join(kws[i : i + kw_len])]

    txt = ' '.join(words)
    for combination in combinations:
        frequencies[combination] = txt.count(combination)

    lst = sorted(frequencies.iteritems(), key=operator.itemgetter(1), reverse=True)

    for i, val in enumerate(lst):
        word, freq = val
        #percent = round(float(freq) / summ_words * 100, 2)
        lst[i] = list(lst[i])# + [percent]

    lst = lst[:limit]

    return lst


def select_relevant_sents(query, sents, count, morph, norm_words_blacklist):

    norm_query_words = uniq(_normalize_to_words(query, morph, norm_words_blacklist))
    sents_with_freq_points = []

    for sent in sents:
        norm_sent = uniq(_normalize_to_words(sent, morph, norm_words_blacklist))

        if set(norm_query_words) == set(norm_sent):
            continue

        freqs = _frequency(norm_query_words, morph, words=norm_sent)
        freq_points = len([f for f in freqs if f[1] > 0])
        sents_with_freq_points.append((sent, freq_points))

    sents_with_freq_points = sorted(sents_with_freq_points,
                                    key=operator.itemgetter(1),
                                    reverse=True)
    #pr(sents_with_freq_points)
    sents = [s[0] for s in sents_with_freq_points[:count]]
    return sents


