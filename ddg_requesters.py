#!/usr/bin/env python

import requests
from itertools import cycle
import datetime
import json

import logging, coloredlogs
coloredlogs.install()
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


#DEBUG requests
import http.client as http_client
http_client.HTTPConnection.debuglevel = 1
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


from lib.proxies import Proxies
from lib.decorators import time_limit
from lib.base import sentences


REQUESTERS = sorted([
    'candeloops', 'eproworld', 'magnaplatecn',
    'megeventos', 'mycarpetking', 'wmedwardflorist',
])

IMAGES_JSURL = 'https://www.dropbox.com/s/k5fcn4t4pt8btof/ddg_images.js?dl=1&version=1'


class DDG_Not_Found(Exception):
    pass
class DDG_Error(Exception):
    pass
class DDG_RPS_Limit(Exception):
    pass



class DDG(object):

    proxies = None
    requesters_1_half = None
    requesters_2_half = None

    def __init__(self, proxies_listname='ddg_default'):
        self.proxies = Proxies(proxies_listname)
        self.requesters_1_half = cycle(REQUESTERS[:round(len(REQUESTERS)/2)])
        self.requesters_2_half = cycle(REQUESTERS[round(len(REQUESTERS)/2):])


    def _get_requester(self):
        if datetime.datetime.now(tz=datetime.timezone.utc).hour < 12:
            app = next(self.requesters_1_half)
        else:
            app = next(self.requesters_2_half)
        return "https://{}.herokuapp.com/phantomjs/".format(app)


    def images(self, query):
        data = None

        while data is None:
            try:

                out = requests.post(
                    url=self._get_requester(),
                    data={
                        'jsurl': IMAGES_JSURL,
                        'proxy': self.proxies.current(),
                        'args': json.dumps([query]),
                        'timeout': 16,
                    },
                    timeout=32,
                ).content

                #logger.info(result.stdout);return

                if isinstance(out, bytes):
                    out = out.decode()

                if "DDG.duckbar.failed('images')" in out:
                    raise DDG_Not_Found(query)

                try:
                    data = json.loads(out)
                except (json.JSONDecodeError, ) as e:
                    logger.error(e)
                    logger.info(out)
                    self.proxies.randomize_proxy()
                    data = None
                    continue

            except DDG_Not_Found as e:
                logger.info(e)
                raise e
            except Exception as e:
                logger.info(e)
                self.proxies.randomize_proxy()
                data = None
                continue

        data['snippets_sentences'] = []
        for i, snippet in enumerate(data['snippets']):
            data['snippets_sentences'] += sentences(snippet)



        return data


if __name__ == '__main__':
    ddg = DDG()
    print(dd.images('test'))