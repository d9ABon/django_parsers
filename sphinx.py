import hashlib
from djangosphinx.models import SearchError
from djangosphinx.models import SphinxQuerySet as SphinxQuerySet_original
from djangosphinx.apis import current as sphinxapi
from settings import SPHINX_SERVER  #,SPHINX_PORT)


def get_sphinx_port_number(index):
    FREE_PORT_NUMBERS = (49152, 65535)
    index_int_representation = sum([ord(c) for c in hashlib.md5(index).hexdigest()])
    port = index_int_representation % (FREE_PORT_NUMBERS[1] - FREE_PORT_NUMBERS[0]) + FREE_PORT_NUMBERS[0]
    return port


class SphinxQuerySet(SphinxQuerySet_original):

    def _get_sphinx_client(self):
        client = sphinxapi.SphinxClient()
        #client.SetServer(SPHINX_SERVER, SPHINX_PORT)
        client.SetServer(SPHINX_SERVER, get_sphinx_port_number(self._index))
        return client

