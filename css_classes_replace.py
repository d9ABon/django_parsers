import os
import re
import random
import json

"""
#inspired by https://github.com/ccampbell/html-muncher/
"""

def uniq(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]

def _looks_like_colour(s):
    return re.match(r'^#[0-9a-fA-F]{3}|#[0-9a-fA-F]{6}$', s, flags=re.DOTALL | re.IGNORECASE)

def _repl_identifiers_in_css(css, key, value):
    css = css.replace(key + "{", value + "{")
    css = css.replace(key + " {", value + " {")
    css = css.replace(key + "#", value + "#")
    css = css.replace(key + " #", value + " #")
    css = css.replace(key + " >", value + " >")
    css = css.replace(key + " <", value + " <")
    css = css.replace(key + ".", value + ".")
    css = css.replace(key + " .", value + " .")
    css = css.replace(key + ",", value + ",")
    css = css.replace(key + " ", value + " ")
    css = css.replace(key + ":", value + ":")
    return css



class CssRepl(object):
    css = None
    html = None
    css_file = None
    css_manifest = None

    def __init__(self, css_file=None, css_manifest=None):
        self.css_file = css_file
        self.css_manifest = css_manifest


    def make_css_manifest(self):
        self.css_manifest = {'ids':{}, 'classes':{}}
        with open(self.css_file) as f:
            css = f.read()

        alphabet1 = [chr(c) for c in range(ord('a'), ord('z') + 1) + range(ord('A'), ord('Z') + 1)]
        alphabet2 = alphabet1[:]
        random.shuffle(alphabet2)
        char_repl_table = dict(zip(alphabet1, alphabet2) +
                               zip(map(str, range(0,10)), map(str, range(0,10))) +
                               zip(['_', '-', ' '],  ['_', '-', ' '])
        )

        ids_found = re.findall(r'((?<!\:\s)(?<!\:)#\w+)(\.|\{|,|\s|#)', css, flags=re.DOTALL | re.IGNORECASE | re.MULTILINE)
        ids_found = uniq([css_id for css_id, nextchar in ids_found if not _looks_like_colour(css_id)])

        classes_found = uniq(re.findall(r'(?!\.[0-9])\.[\w-]+', css, flags=re.DOTALL | re.IGNORECASE | re.MULTILINE))

        for css_id in ids_found:
            self.css_manifest['ids'][css_id] = '#' + ''.join([char_repl_table[c] for c in css_id[1:]])
        for css_class in classes_found:
            self.css_manifest['classes'][css_class] = '.' + ''.join([char_repl_table[c] for c in css_class[1:]])

        self.css_manifest['custom_class_selectors'] = uniq(re.findall('\[class.?="([a-zA-Z0-9-_\s]+)"\]', css, flags=re.DOTALL | re.IGNORECASE | re.MULTILINE))
        self.css_manifest['custom_id_selectors'] = uniq(re.findall('\[id.?="([a-zA-Z0-9-_\s]+)"\]', css, flags=re.DOTALL | re.IGNORECASE | re.MULTILINE))
        self.css_manifest['char_repl_table'] = char_repl_table
        return self

    #see https://stackoverflow.com/questions/28664383/mongodb-not-allowing-using-in-key
    def css_manifest_to_mongo(self):
        manifest = {}
        for group in self.css_manifest.keys():
            if not isinstance(self.css_manifest[group], dict):
                manifest[group] = self.css_manifest[group]
                continue

            manifest[group] = {}
            for key, value in self.css_manifest[group].items():
                if '.' in key:
                    key = key.replace('.', '!')
                if key[0] == '$':
                    key[0] = '&'
                manifest[group][key] = value
        return manifest
    def css_manifest_from_mongo(self, mongo_manifest):
        self.css_manifest = {}
        for group in mongo_manifest.keys():
            if not isinstance(mongo_manifest[group], dict):
                self.css_manifest[group] = mongo_manifest[group]
                continue

            self.css_manifest[group] = {}
            for key, value in mongo_manifest[group].items():
                if '.' in key:
                    key = key.replace('!', '.')
                if key[0] == '&':
                    key[0] = '$'
                self.css_manifest[group][key] = value
        return  self.css_manifest

    def parse_css(self):
        with open(self.css_file) as f:
            css = f.read()

        if not self.css_manifest:
            self.make_css_manifest()

        for key, value in self.css_manifest['classes'].items():
            css = _repl_identifiers_in_css(css, key, value)

        for key, value in self.css_manifest['ids'].items():
            css = _repl_identifiers_in_css(css, key, value)

        for css_class in self.css_manifest['custom_class_selectors']:
            css = re.sub(r'"%s"' % css_class, '"%s"' % ''.join([self.css_manifest['char_repl_table'][c] for c in css_class[:]]), css, flags=re.DOTALL | re.MULTILINE)

        for css_class in self.css_manifest['custom_id_selectors']:
            css = re.sub(r'"%s"' % css_class, '"%s"' % ''.join([self.css_manifest['char_repl_table'][c] for c in css_class[:]]), css, flags=re.DOTALL | re.MULTILINE)

        self.css = css
        return self

    def replace_html(self, html):
        for key, value in self.css_manifest['ids'].items():
            key = key[1:]
            value = value[1:]
            html = html.replace("id=\"" + key + "\"", "id=\"" + value + "\"")

        for class_block, classes in re.findall(r'''(class=['"]([^'"]+)['"])''', html, flags=re.MULTILINE | re.DOTALL | re.IGNORECASE):
            classes_lst = re.split('\s+', classes, flags=re.MULTILINE | re.DOTALL | re.IGNORECASE)
            for i, cls in enumerate(classes_lst):
                if '.%s' % cls in self.css_manifest['classes']:
                    classes_lst[i] = self.css_manifest['classes']['.%s' % cls][1:]
            html = html.replace(class_block, class_block.replace(classes, ' '.join(classes_lst)))

        return html

    def replace_html_files(self, html_files_list):
        if __name__ != '__main__':
            from lib.progress_meter import ProgressMeter
            pm = ProgressMeter(total=len(html_files_list), unit='html files', rate_refresh=60)

        for html_file in html_files_list:
            # print 'processing %s' % html_file
            with open(html_file) as f:
                html = f.read().decode('utf-8')

            html = self.replace_html(html)

            with open(html_file, 'w+') as f:
                f.write(html)

            if __name__ != '__main__':
                pm.update(1)


if __name__ == '__main__':
    import sys
    reload(sys)
    sys.setdefaultencoding("utf-8")


    CssRepl('/tmp/loadsystem.stationadmin.ru/static/0/css/style.css').process_html(['/tmp/loadsystem.stationadmin.ru/creative-sb-live-vista-7-driver-pack'])


