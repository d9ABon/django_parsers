#!/usr/bin/env python

import StringIO
import lxml
import lxml.html as lh
import re


def get_document_tree(html):
    if not len(html):
        print 'not len(html)'
    # from feedparser import _sanitizeHTML
    # html = clean_html(_sanitizeHTML(result.content, 'utf-8', 'text/html'))
    # inner_html = re.findall('<body[^>]*>(.*)</body>', html, re.MULTILINE | re.DOTALL)[0]
    # import pdb;pdb.set_trace()
    # html = '<!DOCTYPE html><html><body>%s</body></html>' % html

    parser = lxml.etree.HTMLParser()
    try:
        tree = lxml.etree.parse(StringIO.StringIO(html), parser=parser)
    except Exception as e:
        tree = lxml.html.fromstring(html)

    html = lxml.etree.tostring(tree)

    doc = lh.fromstring(html)
    return doc

"""
  "url": { "css": "#comic img", "value": "@src" },
  "title": { "css": "#comic img", "value": "@title" },
  "body_text": { "css": "div.main", "value": ".//text()" }

   To extract the innerHTML, use "./node()"; and to extract the outer HTML, use ".".

    "title": { "path": "results.data[*].title" },
  "description": { "path": "results.data[*].description" }

    "word": { "regexp": "^(.+?): (.+)$", index: 1 },
  "definition": { "regexp": "^(.+?): (.+)$", index: 2 }
"""

def fetch_html_elements_by_css_path(html, elements_dict, join_str = "\n", page_url=None):
    doc = get_document_tree(html)
    data = {}


    def _legacy(elements_dict):
        for name, css_path in elements_dict.items():
            elements_dict[name] = css_path.replace('***(HTML)***', ' //node()')
            elements_dict[name] = css_path.replace('***(LINK)***', ' //link()')
            elements_dict[name] = re.sub(r'\*\*\*\(ATTR=([^\)]+)\)\*\*\*', lambda m: r' //attr(%s)' % m.group(1), css_path)
        return elements_dict
    elements_dict = _legacy(elements_dict)



    def _node(tags):
        return [lh.tostring(tag) for tag in tags]
    def _link(tags):
        return ["[[%s|%s]]" % (tag.attrib['href'], tag.text_content().replace('|', '/')) for tag in tags]
    def _attr(tags, attr):
        return [tag.attrib[attr] for tag in tags]
    def _remove(tags):  #works incorrect with text nodes besides removed tags, use //clear
        for tag in tags:
            tag.getparent().remove(tag)
    def _clear_text(tags):
        for tag in tags:
            tag.text = ''
    def _phantomjs(url, css_path, attribute, valueValidation=None):
        import subprocess
        import os
        from settings import PROJECT_PATH
        if url[:2] == '//':
            url = 'http:'+url
        cmd = [os.environ['PHANTOMJS'], os.path.join(PROJECT_PATH, 'parsers', 'page_element.js'), url, '"%s"' % css_path, attribute]
        if valueValidation:
            cmd += [valueValidation]

        stdout, stderr = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()

        try:
            value = re.search(r'VALUE:<!\[CDATA\[(.*)\]\]>', stdout, flags=re.DOTALL | re.MULTILINE | re.IGNORECASE).groups()[0]
        except Exception:
            value = None

        return value


    def process(css_path, doc):
        value = []
        nth_child = None
        regexp_match = None

        css_path = css_path.strip()
        if not css_path:
            return


        if '&&' in css_path:
            css_path_list = [c.strip() for c in css_path.split('&&')]
            css_path = css_path_list[0]+'//node()'
            nodes = process(css_path, doc)
            for i, css_path in enumerate(css_path_list[1:]):
                nodes_doc = get_document_tree(nodes)
                if i < len(css_path_list) - 2:  #not last
                    css_path += '//node()'
                nodes = process(css_path, nodes_doc)
            return nodes

        if ':nth-child(' in css_path:
            nth_child = re.search(':nth-child\(([^\)]+)\)', css_path).groups()[0]
            css_path = re.sub(':nth-child\([^\)]+\)', '', css_path).strip()

        if '//remove(' in css_path: #works incorrect with text nodes besides removed tags, use //clear_text
            remove = re.search('//remove\(([^\)]+)\)', css_path).groups()[0]
            css_path = re.sub('//remove\([^\)]+\)', '', css_path).strip()
            _remove(doc.cssselect(remove))

        if '//clear_text(' in css_path:
            clear_text = re.search('//clear_text\(([^\)]+)\)', css_path).groups()[0]
            css_path = re.sub('//clear_text\([^\)]+\)', '', css_path).strip()
            _clear_text(doc.cssselect(clear_text))

        if '//regexp_match|' in css_path:
            regexp_match = re.search('//regexp_match\|([^\|]+)\|', css_path).groups()[0]
            css_path = re.sub('//regexp_match\|[^\|]+\|', '', css_path).strip()




        if css_path == 'EMPTY':
            value += ['']
        elif '//regexp_html|' in css_path:
            regexp_html = re.search('//regexp_html\|([^\|]+)\|', css_path).groups()[0]
            html = lh.tostring(doc)
            match = re.search(regexp_html, html, flags=re.S|re.M|re.I)
            #import ipdb;ipdb.set_trace()
            if match:
                value += [match.groups()[0]]
            else:
                value += ['']
        elif '//text(' in css_path:
            value += [re.search('//text\(([^\)]+)\)', css_path).groups()[0]]
        elif '//phantomjs(' in css_path:
            if '//attr(' in css_path:
                attr = re.search('//attr\(([^\)]+)\)', css_path).groups()[0]
                css_path = re.sub('//attr\([^\)]+\)', '', css_path).strip()
            else:
                attr = 'html'
            css_path = re.sub('//phantomjs\(([^\)]+)\)', lambda matches:matches.group(1), css_path).strip()
            value += [_phantomjs(page_url, css_path, attr)]
        elif '//node(' in css_path:
            css_path = css_path.replace('//node()', '').strip()
            value += _node(doc.cssselect(css_path))
        elif '//link(' in css_path:
            css_path = css_path.replace('//link()', '').strip()
            value += _link(doc.cssselect(css_path))
        elif '//attr(' in css_path:
            attr = re.search('//attr\(([^\)]+)\)', css_path).groups()[0]
            css_path = re.sub('//attr\([^\)]+\)', '', css_path).strip()
            value += _attr(doc.cssselect(css_path), attr)
        else:
            value += [tag.text_content() for tag in doc.cssselect(css_path)]

        if nth_child:
            if ':' in nth_child:
                start, end = [int(i) if i else None for i in nth_child.split(':')]
                value = value[start:end]
            else:
                value = [value[int(nth_child)-1]]  #starts with 1


        if regexp_match:
            import ipdb;ipdb.set_trace()
            value = [v for v in value if re.match(regexp_match, v, flags=re.S|re.M)]

        return join_str.join([v for v in value if v and len(str(v).strip())])


    for name, css_path in elements_dict.items():
        data[name] = process(css_path, doc)

    return data


if __name__ == "__main__":
    import sys
    reload(sys)
    sys.setdefaultencoding("utf-8")

    if len(sys.argv) != 3:
        print "%s <url> <css_path>" % (sys.argv[0],)
        sys.exit()

    import requests
    html = requests.get(sys.argv[1]).content
    result = fetch_html_elements_by_css_path(html, {'test':sys.argv[2]})
    print result['test']