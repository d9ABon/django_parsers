# -*- coding: utf-8 -*-
import os
import string
from settings import PROJECT_PATH
import operator
from django.core.cache import cache
from hashlib import md5
from lib.php import php
from lib.base import pr, uniq
import time
import re

stopwords = [u'в', u'на', u'и', u'не', u'с', u'что', u'для', u'к', u'за', u'по', u'а', u'В', u'о', u'же', u'от',]
stopwords += [u'как', u'Наш', u'то', u'–', u'так', u'он', u'но', u'—', u'при', u'все', u'c', u'его']
stopwords += [u'или', u'после', u'ваших', u'Наш', u'icq', u'тел', u'c', u'ru', u'у', u'уже', u'это', u'И', ]
stopwords += [u'из', u'Если', u'было', u'этого', u'др', u'бы', u'см', u'то', u'чем', u'нет', u'если', ]
stopwords += [u'м', u'А', u'во', u'без', u'Так', u'быть', u'под', u'Но', u'их', u'них', u'и', u'Как', ]
stopwords += [u'этом', u'ему', u'Это', u'Все', u'К', u'ни', u'этот', u'этой', u'им', u'того',]
stopwords += [u'мы', u'самых', u'него', u'тоже', u'свое', u'свой', u'тем', u'была', u'млн', u'Для',]
stopwords += [u'когда', u'С', u'М', u'самого', u'которой', u'сам', u'При', u'Очень', u'особо', u'т', ]
stopwords += [u'Если', u'Для', u'только', u'вам', u'Это', u'будет', u'может', u'При', u'И', u'я', ]
stopwords += [u'также', u'вас', u'А', u'того', u'чтобы', u'Не', u'Как', u'Все', u'Вы', u'ее', ]
stopwords += [u'', u'', u'В', u'c', u'Если', u'Для' u'Есть', u'Всего',u'Но', u'На', u'до',]
stopwords += [u'того', u'со', u'еще', u'На', u'вы', u'можно', u'с', u'ты', u'таких', u'в',]
stopwords += [u'меня', u'того', u'есть', u'всех', u'даже', u'более', u'они',]
stopwords += [u'она', u'меня', u'мень', u'вот', u'тут', u'наш', u'ты', u'тот', u'там', u'это',]
stopwords += [u'мы', u'я', u'наш', u'это']

stopwords += 'the,is,to,and,are,this,of,for,be,or,with,as,in,has,not,if,on,your,will,can'.split(',')
stopwords += 'our,here,from,you,at,it,that,than,there,have,we,an,out,by,all,was,more,very'.split(',')
stopwords += 'over,get,need,use,do,into,it,how,any,also,when'.split(',')
stopwords += 'a,non,+,&'.split(',')

stopwords = map(lambda sw: sw.lower().strip(), stopwords)


def sanitize_and_split_to_words(text):
    # print '%'*10, text
    words = str(text).lower().split(None)

    words = filter(len, words)
    def not_numeric(w):
        for n in'0123456789':
            if n in w:
                return False
        return True
    words = filter(not_numeric, words)
    words = map(str, words)

    words = filter(lambda w: w.lower() not in stopwords, words)
    words = map(lambda w: w.lower().replace('.', '').replace(',', '').replace('?', ''), words)
    words = filter(len, words)

    # for w in words:
    #     print '#', w, isinstance(w, str), isinstance(w, unicode)

    return words


def _recent_average_percent_query_words_are_most_used(current_percent):
    avg_percent, count = cache.get('recent_average_percent_query_words_are_most_used', (0, 0))
    avg_percent = ((avg_percent * count) + current_percent) / (count + 1)
    cache.set('recent_average_percent_query_words_are_most_used', (avg_percent, count + 1), None)
    print 'recent_average_percent_query_words_are_most_used:', avg_percent, 'for', count + 1, 'runs'
    return avg_percent


CHECK_PASSED_IF_PERCENT_QUERY_WORDS_ARE_IN_MOST_USED = 19
def check_query_words_are_most_used(query, matched_lines):
    query_words = normalize_to_words(query)
    if not len(query_words):
        return False
    freq_lst = frequency(query, words=normalize_to_words(matched_lines), limit=20)

    most_used = [elem[0] for elem in freq_lst]
    print 'most used'
    pr(most_used)

    print 'query words'
    pr(query_words)

    intersection = [val for val in query_words if val in most_used]

    percent = float(len(intersection)) / len(query_words) * 100
    print 'intersection=', percent

    check_passed = percent >= CHECK_PASSED_IF_PERCENT_QUERY_WORDS_ARE_IN_MOST_USED

    _recent_average_percent_query_words_are_most_used(percent)  #for now just checking

    if not check_passed:
        time.sleep(1)  #stub

    return check_passed



def frequency(query, text=None, words=None, limit=10):
    query_words = normalize_to_words(query)

    table = string.maketrans("","")

    if not words:
        words = normalize_to_words(text)

    #translate cannot take unicode
    words = map(lambda w: w.encode('utf-8').translate(table, string.punctuation).decode('utf-8'), words)
    words = filter(len, words)

    frequencies = {}
    for word in words:
        frequencies[word] = frequencies.get(word, 0) + 1

    summ_words = sum(frequencies.values())




    #make all possible kws combinations
    #ex. from keywords ['a','b','c','d'] ==> ['a b', 'b c', 'c d', 'a b c', 'b c d', 'a b c d']
    kws = query_words
    combinations = []
    for kw_len in range(2, 5):  #min 2 words, max 4 words
        for i in range(len(kws) - kw_len + 1):
            combinations += [' '.join(kws[i : i + kw_len])]

    txt = ' '.join(words)
    for combination in combinations:
        frequencies[combination] = txt.count(combination)





    lst = sorted(frequencies.iteritems(), key=operator.itemgetter(1), reverse=True)

    for i, val in enumerate(lst):
        word, freq = val
        percent = round(float(freq) / summ_words * 100, 2)
        lst[i] = list(lst[i]) + [percent]

    lst = lst[:limit]


    return lst






CIPFA_NUM_FIRST_WORDS = 4  #was:6
def cipfa(text=None, words=None, whitelist=None):
    table = string.maketrans("","")

    if not words:
        words = normalize_to_words(text)


    #translate cannot take unicode
    words = map(lambda w: w.encode('utf-8').translate(table, string.punctuation).decode('utf-8'), words)
    frequencies = {}
    for word in words:
        frequencies[word] = frequencies.get(word, 0) + 1

    if '' in frequencies:
        del frequencies['']


    full_lst = sorted(frequencies.iteritems(), key=operator.itemgetter(1), reverse=True)
    lst = full_lst[:CIPFA_NUM_FIRST_WORDS]  #cipfa for N first words


    if whitelist:

        freq_words = [f[0] for f in lst]
        #print 'freq_words';pr(freq_words)
        for word in whitelist:
            if word not in frequencies: #unicode error
                continue
            if word not in freq_words:  #we need to add query word to list
                if word in words:  #if this word exists in text - use it's real frequency, else 0
                    lst += [(word, frequencies[word])]
                else:
                    lst += [(word, 0)]
                #word will be moved to the top of list below


        for elem in reversed(lst[:]): #backwards iterate lst
            if elem[0] in whitelist:
                # print "found", elem, 'removing', lst.index(elem), 'element from lst, which ==', lst[lst.index(elem)]
                # import ipdb;ipdb.set_trace()
                del lst[lst.index(elem)]
                lst.insert(0, elem)  #move whitelist word to the top of list


    freq_words = [f[0] for f in lst]
    # print 'freq_words';pr(freq_words)
    # print 'whitelist';pr(whitelist)
    # print 'lst';pr(lst)

    real_freqs = [freq for word, freq in lst]
    # print real_freqs

    first_word_freq = lst[0][1]

    eligible_freqs = []
    for i in range(len(lst)):
        elem = lst[i]
        if i == 0 or (whitelist and elem[0] in whitelist):
            eligible_freq = real_freqs[i]
            eligible_freq = max(eligible_freq, 15)
        else:
            eligible_freq = int(round(float(first_word_freq) / (i + 1)))

        eligible_freqs += [eligible_freq]



    # print eligible_freqs


    recommendations = {}
    for i, val in enumerate(lst):
        word, freq = val
        recommendations[word] = eligible_freqs[i] - real_freqs[i]




    def join_words_that_looks_the_same_in_rec(recommendations):
        #join words that looks the same
        def sort_by_len(w1, w2):
            if len(w1) > len(w2):
                return w2, w1
            return w1, w2

        # print 'before:', recommendations
        norm_re = lambda s: re.sub('([^\w\d]+)', '', s)
        words = recommendations.keys()
        norm = map(norm_re, words)
        words_assoc_with_normwords = dict(zip(norm, words))

        join_elements = {}  # {leave_w:join_w, ...}
        for i, norm_w in enumerate(norm):
            if len(norm_w) <= 3:
                continue
            if norm_w in join_elements.keys() or norm_w in join_elements.values():
                continue
            for other_w in norm[:i] + norm[i+1:]:  #list except current element
                if other_w in join_elements.keys() or other_w in join_elements.values():
                    continue
                if other_w[:6] in norm_w:
                    #removing longer word
                    pair = sort_by_len(norm_w, other_w)
                    join_elements[pair[0]] = pair[1]
        # print 'join_elements:', join_elements
        for w1, w2 in join_elements.items():
            try:
                orig_w1 = words_assoc_with_normwords[w1]
                orig_w2 = words_assoc_with_normwords[w2]
                if orig_w1 in recommendations and orig_w2 in recommendations:
                    recommendations[orig_w1] += recommendations[orig_w2]
                    del recommendations[orig_w2]
            except Exception as e:
                print e
                import ipdb;ipdb.set_trace()

        # print 'after:', recommendations
        return recommendations

    recommendations = join_words_that_looks_the_same_in_rec(recommendations)




    print 'recommendations:'
    pr(recommendations)


    # curr = max_freq
    # for i, val in enumerate(lst):
    #     word, freq = val
    #     eligible =


    # print frequencies
    # for word in keys[:1000]:
    #     print word, frequencies[word]

    # time.sleep(10)

    return recommendations



def normalize_to_words(sent):
    if isinstance(sent, list):
        ret = []
        for line in sent:
            ret += normalize_to_words(line)
        return ret

    cache_key = 'cipfa_normalize_to_words_%s' % hash(sent)
    ret = cache.get(cache_key)
    if ret is not None:
        return ret

    words = sanitize_and_split_to_words(sent)

    global_vars = {'words': map(lambda w: w.upper(), words), }
    files = ['./pymorphy.php',]
    code = """ pymorphy($words) """
    try:
        data = php(code, files, global_vars)
    except Exception as e:
        print e
        return words

    if isinstance(data, list):
        print 'cannot normalize', words
        return words  #cannot normalize?

    # for w, nw in data.items():
    #     print w, isinstance(w, str), isinstance(w, unicode)
    #     print nw, isinstance(nw, str), isinstance(nw, unicode)

    normalise_dict = dict((str(w).lower(), str(nw).lower()) for w, nw in data.items())
    ret = [normalise_dict[w] for w in words]

    cache.set(cache_key, ret, 60)

    return ret