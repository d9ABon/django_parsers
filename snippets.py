# -*- coding: utf-8 -*-
import random
from hashlib import md5
import re

from lib.php import php

from settings import SNIPPET_SES

from django.core.cache import cache


def _get_snippets(query):
    snippet_se = random.choice(SNIPPET_SES)

    php_vars = {'q': query, 'query': query}

    files = ['../seodor/conf.php', '../seodor/inc/functions.php',
             '../seodor/inc/mc.php', '../seodor/inc/parsers/%s.php' % snippet_se]
    snippets = php("""%s($query, 'ru')""" % snippet_se, files, php_vars)

    if snippets is None or not len(snippets):
        print 'BAD SE:', snippet_se
        return False

    #import ipdb;ipdb.set_trace()
    #print 'len snippets:', len(snippets)

    return snippets


def get_snippets(query):
    if isinstance(query, unicode):
        query = query.encode('utf-8')
    cache_key = 'snippets_by_query_%s' % md5(query).hexdigest()
    snippets = cache.get(cache_key)
    # if snippets is not None:
    #     return snippets

    tries = 0
    snippets = _get_snippets(query)
    while not snippets and tries < 10:
        snippets = _get_snippets(query)
        tries += 1



    for key, snippet in enumerate(snippets):
        title, description = snippet.split(' * ')
        snippets[key] = {'title': title, 'description': description}


    if len(snippets):
        cache.set(cache_key, snippets, 60 * 60 * 24 * 7)


    return snippets


"""

if ($stopWords) $content = stopWords($content);

	if ($filterSnipp) { // фильтр "плохих" сниппетов из файла filterSnipp.txt
		$filter = array_filter(explode("\n",file_get_contents($ucontent.'/filterSnipp.txt', FILE_USE_INCLUDE_PATH)));
		foreach($filter as $del) {
			$content = filterSnipp($content, str_replace("\r", "", $del));
		}
	}

	if ($goodSnipp) { // парсинг только тех сниппетов, которые содержат слова из $query
		$filter = array_filter(preg_replace("~[^0-9a-zа-яё]+~iu", "", explode(" ", $query)));
		foreach($filter as $good) {
			$content = goodSnipp($content, $good);
		}
	}
"""