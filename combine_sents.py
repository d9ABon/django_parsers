from parsers.words_freq_deprecated import cipfa, normalize_to_words, frequency, check_query_words_are_most_used
from tsv import generate_tsv
import csv
import random
from lib.base import pr, uniq, flattern_list
from lib.decorators import retry_on_exception, time_limit, TimeoutException
from lib.log import get_logger
from parsers.sphinx import SphinxQuerySet, SearchError
from parsers.wordstat import get_keywords, WordstatError
import operator


#check if a list contains another list
"""
def contains(small, big):
    for i in xrange(len(big)-len(small)+1):
        for j in xrange(len(small)):
            if big[i+j] != small[j]:
                break
        else:
            return i, i+len(small)
    return False
"""
def in_a(needle, lst):  #needle can be one word or new words
    if ' ' in needle:
        #import ipdb;ipdb.set_trace()
        return needle in ' '.join(lst)
    else:
        return needle in lst


def _search(query, gentext_lines, sphinx_index_name, limit=40):
    #pr('_search:', query)
    search = SphinxQuerySet(index=sphinx_index_name, limit=40, passages=True, mode='SPH_MATCH_ANY', rankmode='SPH_RANK_NONE')
    results = search.query(query)

    #@retry_on_exception(Exception, retries=15, interval=1.0, exponent=1.1)  #may be not only SearchError
    def _get_results_list(results, limit):
        return results[0:limit]

    try:
        results_list = _get_results_list(results, limit)
    except Exception as e:
        raise SearchError(str(e))  #make this unknown exception SearchError

    matched_lines = [gentext_lines[r['id']].decode('utf-8') for r in results_list]


    # opts = {}
    # words = [w['word'] for w in results._sphinx.get('words')]
    # lines = [line.encode('utf-8') for line in matched_lines]
    # passages_list = results._get_sphinx_client().BuildExcerpts(lines, results._index, ' '.join(words), opts)
    # for p in passages_list:
    #     print p

    return matched_lines


def related_search(query, gentext_lines, sphinx_index_name, limit=40):
    matched_lines = []

    with time_limit(10):
        try:
            related_kws = get_keywords(query)['more_with_this_keyword']
        except (WordstatError, TimeoutException) as e:
            print e
            return matched_lines

    # print 'related'
    # pr(related_kws)

    if not related_kws:
        return matched_lines
    else:
        related_kws = sorted(related_kws.iteritems(), key=operator.itemgetter(1), reverse=True)
        related_kws = [rkw[0] for rkw in related_kws]
        related_kws_copy = related_kws[:]

    while len(matched_lines) < limit and len(related_kws):
        query = related_kws.pop(0)
        matched_lines += _search(query, gentext_lines, limit=limit, sphinx_index_name=sphinx_index_name)



    lists_of_words = map(lambda q: q.split(), related_kws_copy + [query])
    all_words = uniq(flattern_list(lists_of_words))
    random.shuffle(all_words)

    while len(matched_lines) < limit and len(all_words):
        matched_lines += _search(' '.join(all_words), gentext_lines, limit=limit, sphinx_index_name=sphinx_index_name)
        all_words.pop()




    return matched_lines[:limit]


class CannotFindWordInText(Exception): pass
class BugWithGentextLines(Exception): pass

def _remove_matched_lines(matched_lines, word, count_diff, gentext_lines, add_line_for_ballance=True):
    #print 'remove matched line with word:', word, 'count_diff=', count_diff
    while count_diff < 0 and len(matched_lines) > 0:
        removed_at_least_one = False

        #cycle all matched_lines in random order
        indexes = range(len(matched_lines))
        random.shuffle(indexes)
        for i in indexes:
            line = matched_lines[i]

            words = normalize_to_words(line)

            if in_a(word, words):
                del matched_lines[i]
                count_diff += 1
                removed_at_least_one = True

                if add_line_for_ballance:
                    try:
                        matched_lines += [random.choice(gentext_lines).decode('utf-8')]  #for ballance
                    except KeyError as e:  #strange bug! random.choice broken with dicts.  why is this a dict??
                        get_logger('gentext_lines_is_dict').error(e)
                        get_logger('gentext_lines_is_dict').error('len(gentext_lines): %d' % len(gentext_lines))
                        get_logger('gentext_lines_is_dict').error('gentext_lines is instance of list: %s', isinstance(gentext_lines, list))
                        get_logger('gentext_lines_is_dict').error('gentext_lines is instance of dict: %s', isinstance(gentext_lines, dict))
                        raise MiserableKeyword()

                break

        if not removed_at_least_one:  #something is wrong, cannot find word to delete in matched_lines
            raise CannotFindWordInText('not removed_at_least_one!!!')

    return matched_lines



def _add_lines_with_word(matched_lines, word, count_diff, gentext_lines, sphinx_index_name):
    additional_matched_lines = _search(word, gentext_lines, limit=40, sphinx_index_name=sphinx_index_name)
    print '_add_lines_with_word:', word, 'count_diff=', count_diff, 'len(additional_matched_lines):', len(additional_matched_lines)

    if not len(additional_matched_lines):
        #print 'not len(additional_matched_lines)'
        return matched_lines

    while count_diff > 0 and len(additional_matched_lines):
        #get random search result
        line = additional_matched_lines.pop(random.randint(0, len(additional_matched_lines) - 1))

        if line not in matched_lines:
            matched_lines += [line]
            count_diff -= 1
            #print 'additional:', line
        else:
            #print 'already in matched_lines:', line
            pass

    return matched_lines




def _combine_cipfa(query, matched_lines, gentext_lines, sphinx_index_name):
    MAX_TRIES_COUNT = 1
    whitelist = normalize_to_words(query)

    cipfa_recommendations = cipfa(words=normalize_to_words(matched_lines),
                                  whitelist=whitelist)

    def result_looks_pretty_like_recommendations():
        #return cipfa_recommendations.values() == [0] * len(cipfa_recommendations)  #all zeros
        summ_values = sum(map(abs, cipfa_recommendations.values()))
        DEVIATION_NORM = 0.15  # 15%
        deviation_val = round(summ_values * DEVIATION_NORM)
        print 'summ_values:', summ_values, 'deviation_val:', deviation_val

        for k, v in cipfa_recommendations.items():
            if not -2 <= v <= 2:
                return False
        print 'result_looks_pretty_like_recommendations == True'
        return True

    tries_count = 0

    while not result_looks_pretty_like_recommendations() and tries_count <= MAX_TRIES_COUNT:
        recommendations = cipfa_recommendations.items()
        random.shuffle(recommendations)
        for word, count_diff in recommendations:
            #print 'word:', word, 'count_diff:', count_diff
            if count_diff == 0:
                continue
            elif count_diff > 0:
                matched_lines = _add_lines_with_word(matched_lines, word, count_diff, gentext_lines, sphinx_index_name=sphinx_index_name)
            elif count_diff < 0:
                try:
                    matched_lines = _remove_matched_lines(matched_lines, word, count_diff, gentext_lines, add_line_for_ballance=False)
                except CannotFindWordInText as e:
                    print e
                    pass  #we have tries_count

        print 'tries_count', tries_count
        tries_count += 1

        cipfa_recommendations = cipfa(words=normalize_to_words(matched_lines + [query]),
                                      whitelist=whitelist)


    pr(cipfa_recommendations)

    return matched_lines


KEYWORD_MAX_FREQUENCY_PERCENT = 6.0  #was:6.0
def _combine_kw_freq(query, matched_lines, gentext_lines):
    if not len(matched_lines):
        raise MiserableKeyword('empty matched_lines in _combine_kw_freq')

    freq_lst = frequency(query, words=normalize_to_words(matched_lines + [query]))

    check_one_more_time = False

    try:
        for word, count, percent in freq_lst:
            if percent <= KEYWORD_MAX_FREQUENCY_PERCENT:
                continue
            else:
                matched_lines = _remove_matched_lines(matched_lines, word, -1, gentext_lines)
                check_one_more_time = True
    except CannotFindWordInText:
        return matched_lines

    if check_one_more_time:
        return _combine_kw_freq(query, matched_lines, gentext_lines)

    pr(freq_lst)

    return matched_lines



def _combine_simple(query, matched_lines, gentext_lines, recently_used_lines):
    num = {
        #'matched': random.randint(3,3),
        #'random': random.randint(5,8),
        #'matched': random.randint(40,80),
        #'random': random.randint(5,10),
        'matched': 12,
        'random': 0,
    }
    ret = {
        'matched': [],
        'random': [],
    }

    while len(ret['matched']) < num['matched'] and len(matched_lines):
        line = matched_lines.pop()
        if line not in recently_used_lines:
            ret['matched'] += [line]
            recently_used_lines.add(line)

    if not len(ret['matched']):
        num['random'] += num['matched']

    while len(ret['random']) < num['random']:
        try:
            line = random.choice(gentext_lines)
        except Exception as e:
            print e
            break

        if line not in recently_used_lines:
            ret['random'] += [line]
            recently_used_lines.add(line)


    ret = ret['random'] + ret['matched']


    if len(recently_used_lines) > 50000:
        #print 'len(recently_used_lines)', len(recently_used_lines)
        for i in range(len(recently_used_lines) - 50000):
            recently_used_lines.pop()

    # freq_lst = frequency(query, words=normalize_to_words(ret))
    # pr(ret)
    # pr(freq_lst)
    # import ipdb;ipdb.set_trace()
    return ret



class MiserableKeyword(Exception):
    pass


COMBINE_SEARCH_LEN = 100
COMBINE_MIN_MATCHED_LINES_LEN = 3  #was:15


def combine(query, generated_text, sphinx_index_name, gentext_lines, use_wordstat=False,
            recently_used_lines=None):
    #print "len(gentext_lines)", len(gentext_lines)

    matched_lines = _search(query, gentext_lines, limit=COMBINE_SEARCH_LEN, sphinx_index_name=sphinx_index_name)
    matched_lines = uniq(matched_lines)
    random.shuffle(matched_lines)

    if use_wordstat and len(matched_lines) < COMBINE_MIN_MATCHED_LINES_LEN:
        matched_lines += related_search(query, gentext_lines,
                                        limit=min(COMBINE_SEARCH_LEN, COMBINE_MIN_MATCHED_LINES_LEN),
                                        sphinx_index_name=sphinx_index_name)


    """
    if len(matched_lines) < COMBINE_MIN_MATCHED_LINES_LEN:
        raise MiserableKeyword("%s, len=%d" % (query.encode('utf-8'), len(matched_lines)))

    def _one_pass(matched_lines, gentext_lines):
        try:
            matched_lines = _combine_cipfa(query, matched_lines, gentext_lines, sphinx_index_name=sphinx_index_name)
            matched_lines = _combine_kw_freq(query, matched_lines, gentext_lines)
        except BugWithGentextLines:
            gentext_lines = _get_gentext_lines(generated_text)

        check_passed = check_query_words_are_most_used(query, matched_lines)

        return matched_lines, gentext_lines, check_passed

    PASSES_COUNT = 1
    check_passed = False
    for i in range(PASSES_COUNT):
        print 'PASS #', i + 1, 'of', PASSES_COUNT
        matched_lines, gentext_lines, check_passed = _one_pass(matched_lines, gentext_lines)


    if not check_passed:  #repeat again
        for i in range(PASSES_COUNT):
            print 'ADDITIONAL PASS #', i + 1, 'of', PASSES_COUNT
            matched_lines, gentext_lines, check_passed = _one_pass(matched_lines, gentext_lines)

    if not check_passed:
        raise MiserableKeyword('not check_query_words_are_most_used')
    """

    matched_lines = _combine_simple(query, matched_lines, gentext_lines, recently_used_lines)

    matched_lines = uniq(matched_lines)
    random.shuffle(matched_lines)

    return matched_lines
