#!/usr/bin/env python
# -*- coding: utf8 -*-
import os
import sys
import re
import regex
import csv
import string
import random
import time
from hashlib import md5

if __name__ == '__main__':# and __package__ is None:
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


from lib.base import sentences




def generate_tsv(text_file=None, tsvfile=None, remove_old_tsv=True):
    with open(text_file) as f:
        lines = f.readlines()
    lines = map(lambda s: s.strip(), lines)
    lines = filter(len, lines)

    sents = []
    for line in lines:
        line = unicode(line)
        line = regex.sub(u'[^\p{Cyrillic}0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ ]', '', line)
        line = line.encode('utf-8', errors='replace').decode('utf-8', errors='replace')
        sents += sentences(line)

    if not tsvfile:
        tsvfile = '/tmp/tmp_%s.tsv' % md5(text_file).hexdigest()

    if remove_old_tsv and os.path.exists(tsvfile):
        if __name__ != '__main__':
            print 'removing old tsv file...'
        os.unlink(tsvfile)

    if os.path.exists(tsvfile):
        return tsvfile

    if __name__ != '__main__':
        print 'generate tsv...'

    with open(tsvfile, 'w+') as f:
        writer = csv.writer(f, delimiter="\t", quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        # writer.writerow(['id', 'uid', 'name'])
        for i, sent in enumerate(sents):
            writer.writerow([i + 1, sent.replace("\t", ' ')])

    return tsvfile


if __name__ == "__main__":
    if len(sys.argv) < 2 or not os.path.exists(sys.argv[1]):
        print "usage: tsv.py /path/to/text_file.txt"
        sys.exit(1)

    #bliss
    reload(sys)
    sys.setdefaultencoding("utf-8")


    tsvfile = generate_tsv(text_file=sys.argv[1])

    time.sleep(1)

    with open(tsvfile) as f:
        lines = f.readlines()

    #see http://newbebweb.blogspot.com/2012/02/python-head-ioerror-errno-32-broken.html
    from signal import signal, SIGPIPE, SIG_DFL
    signal(SIGPIPE,SIG_DFL)

    for line in lines:
        print line.strip()






